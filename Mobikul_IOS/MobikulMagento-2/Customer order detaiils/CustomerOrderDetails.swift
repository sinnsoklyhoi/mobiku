//
//  CustomerOrderDetails.swift
//  Magento2MobikulNew
//
//  Created by Webkul  on 23/08/17.
//  Copyright © 2017 Webkul . All rights reserved.
//

import UIKit
import MapKit

class CustomerOrderDetails: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    let defaults = UserDefaults.standard
    var incrementId:String!
    var customerOrderDetailsModel:CustomerOrderDetailsViewModel!
    @IBOutlet weak var tableView: UITableView!
    var dynamicCellHeight:CGFloat = 0
    var dynamicSummaryHeight:CGFloat = 0
    
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var whichApiToProcess: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "AddressUITableViewCell", bundle: nil), forCellReuseIdentifier: "address")
        tableView.register(UINib(nibName: "OrderInfoItemList", bundle: nil), forCellReuseIdentifier: "OrderInfoItemList")
        tableView.register(UINib(nibName: "OrderSummaryCell", bundle: nil), forCellReuseIdentifier: "OrderSummaryCell")
        tableView.register(UINib(nibName: "StatusOrderTableViewCell", bundle: nil), forCellReuseIdentifier: "StatusOrderTableViewCell")
        tableView.register(UINib(nibName: "DeliveryBoyShipmentdetailsCell", bundle: nil), forCellReuseIdentifier: "DeliveryBoyShipmentdetailsCell")
        
        self.tableView.estimatedRowHeight = 250.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.contentInset = UIEdgeInsets.zero
        tableView.separatorColor = UIColor.clear
        whichApiToProcess = ""
        callingHttppApi()
        self.navigationItem.title = GlobalData.sharedInstance.language(key: "orderdetails")
    }
    
    func callingHttppApi(){
        GlobalData.sharedInstance.showLoader()
        
        if whichApiToProcess == "updateTokenToDataBase" {
            var requstParams = [String:Any]()
            requstParams["userId"] = defaults.object(forKey: "customerId") as! String
            requstParams["name"] = defaults.object(forKey: "customerName") as! String
            
            if let avatar = defaults.object(forKey: "profilePicture") as? String   {
                requstParams["avatar"] = avatar
            }else{
                requstParams["avatar"] = ""
            }
            
            requstParams["token"] = deviceTokenData
            requstParams["accountType"] = "customer"
            requstParams["os"] = "ios"
            
            GlobalData.sharedInstance.fetchDataForFirebaseRequest(params: requstParams, currentView: self, url: "https://us-central1-magento-2-delivery-boy.cloudfunctions.net/updateTokenToDataBase"){success,responseObject in
                if success == 1{
                    self.view.isUserInteractionEnabled = true
                    GlobalData.sharedInstance.dismissLoader()
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomerAdminChatViewController") as! CustomerAdminChatViewController
                    vc.otherUserId = "deliveryAdmin"
                    vc.otherUserName = "Admin"
                    vc.accountType = "customer"
                    vc.childIdKey = self.defaults.object(forKey: "customerId") as! String
                    vc.senderId = self.defaults.object(forKey: "customerId") as! String
                    vc.senderDisplayName = self.defaults.object(forKey: "customerName") as! String
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }else if success == 2{
                    GlobalData.sharedInstance.dismissLoader()
                    self.callingHttppApi()
                }
            }
        } else {
            var requstParams = [String:Any]()
            if self.defaults.object(forKey: "currency") != nil{
                requstParams["currency"] = self.defaults.object(forKey: "currency") as! String
            }
            if self.defaults.object(forKey: "storeId") != nil{
                requstParams["storeId"] = self.defaults.object(forKey: "storeId") as! String
            }
            
            requstParams["incrementId"] = incrementId
            GlobalData.sharedInstance.callingHttpRequest(params:requstParams, apiname:"mobikulhttp/customer/orderDetails", currentView: self){success,responseObject in
                if success == 1{
                    
                    print(responseObject)
                    GlobalData.sharedInstance.dismissLoader()
                    self.customerOrderDetailsModel =  CustomerOrderDetailsViewModel(data: JSON(responseObject as! NSDictionary))
                    self.doFurtherProcessingwithresult()
                }else if success == 2{
                    self.callingHttppApi()
                    GlobalData.sharedInstance.dismissLoader()
                }
            }
        }
    }
    
    func doFurtherProcessingwithresult(){
        print("sss",self.customerOrderDetailsModel.customerTotalData[0].label )
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
        
        if customerOrderDetailsModel.customerOrderDetailsModel.isEligibleForDeliveryBoy == 1{
            
            locManager.requestWhenInUseAuthorization()
            
            if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
                CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
                currentLocation = locManager.location
                
                if currentLocation != nil {
                    print(currentLocation.coordinate.latitude)
                    print(currentLocation.coordinate.longitude)
                }
            }
        }
    }
    
    //MARK:- UITableView
    
    func  numberOfSections(in tableView: UITableView) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else if section == 1{
            if customerOrderDetailsModel.customerOrderDetailsModel.billingAddress != ""{
                return 1
            }else{
                return 0
            }
        }else if section == 2{
            if customerOrderDetailsModel.customerOrderDetailsModel.paymentMethod != ""{
                return 1
            }else{
                return 0
            }
        }else if section == 3{
            if customerOrderDetailsModel.customerOrderDetailsModel.shippingAddress != ""{
                return 1
            }else{
                return 0
            }
        }else if section == 4{
            if customerOrderDetailsModel.customerOrderDetailsModel.shippingMethod != ""{
                return 1
            }else{
                return 0
            }
        }else if section == 5{
            return customerOrderDetailsModel.customerOrderList.count
        }else if section == 6{
            if customerOrderDetailsModel.customerOrderDetailsModel.isEligibleForDeliveryBoy == 1{
                return 1
            }else{
                return 0
            }
        }else if section == 7{
            return 1
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 5{
            return UITableViewAutomaticDimension
        }else if indexPath.section == 7{
            return dynamicSummaryHeight + 50
        } else{
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell:StatusOrderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "StatusOrderTableViewCell") as! StatusOrderTableViewCell
            cell.placeOnDateValue.text = customerOrderDetailsModel.customerOrderDetailsModel.orderDate
            cell.statusMessage.text = " "+customerOrderDetailsModel.customerOrderDetailsModel.status+" "
            if customerOrderDetailsModel.customerOrderDetailsModel.status.lowercased() == "pending"{
                cell.statusMessage.backgroundColor = UIColor().HexToColor(hexString: ORANGECOLOR)
            }else if customerOrderDetailsModel.customerOrderDetailsModel.status.lowercased() == "complete"{
                cell.statusMessage.backgroundColor = UIColor().HexToColor(hexString: GREEN_COLOR)
            }else if customerOrderDetailsModel.customerOrderDetailsModel.status.lowercased() == "processing"{
                cell.statusMessage.backgroundColor = UIColor().HexToColor(hexString: GREEN_COLOR)
            }else if customerOrderDetailsModel.customerOrderDetailsModel.status.lowercased() == "cancel"{
                cell.statusMessage.backgroundColor = UIColor().HexToColor(hexString: REDCOLOR)
            }
            
            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 1{
            let cell:AddressUITableViewCell = tableView.dequeueReusableCell(withIdentifier: "address") as! AddressUITableViewCell
            cell.addressLabel.text = customerOrderDetailsModel.customerOrderDetailsModel.billingAddress
            
            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 2{
            let cell:AddressUITableViewCell = tableView.dequeueReusableCell(withIdentifier: "address") as! AddressUITableViewCell
            cell.addressLabel.text = customerOrderDetailsModel.customerOrderDetailsModel.paymentMethod
            
            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 3{
            let cell:AddressUITableViewCell = tableView.dequeueReusableCell(withIdentifier: "address") as! AddressUITableViewCell
            cell.addressLabel.text = customerOrderDetailsModel.customerOrderDetailsModel.shippingAddress
            
            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 4 {
            let cell:AddressUITableViewCell = tableView.dequeueReusableCell(withIdentifier: "address") as! AddressUITableViewCell
            cell.addressLabel.text = customerOrderDetailsModel.customerOrderDetailsModel.shippingMethod
            
            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 5 {
            let cell:OrderInfoItemList = tableView.dequeueReusableCell(withIdentifier: "OrderInfoItemList") as! OrderInfoItemList
            cell.productName.text = customerOrderDetailsModel.customerOrderList[indexPath.row].productName
            cell.priceValue.text = customerOrderDetailsModel.customerOrderList[indexPath.row].price
            cell.subtotalValue.text = customerOrderDetailsModel.customerOrderList[indexPath.row].SubTotal
            var qtyValue = ""
            if customerOrderDetailsModel.customerOrderList[indexPath.row].qty_CanceledValue != 0{
                qtyValue = qtyValue+GlobalData.sharedInstance.language(key: "canceled")+" :"+customerOrderDetailsModel.customerOrderList[indexPath.row].qty_Canceled+"\n"
            }
            if customerOrderDetailsModel.customerOrderList[indexPath.row].qty_OrderedValue != 0{
                qtyValue = qtyValue+GlobalData.sharedInstance.language(key: "ordered")+" :"+customerOrderDetailsModel.customerOrderList[indexPath.row].qty_Ordered+"\n"
            }
            if customerOrderDetailsModel.customerOrderList[indexPath.row].qty_RefundedValue != 0{
                qtyValue = qtyValue+GlobalData.sharedInstance.language(key: "refunded")+" :"+customerOrderDetailsModel.customerOrderList[indexPath.row].qty_Refunded+"\n"
            }
            if customerOrderDetailsModel.customerOrderList[indexPath.row].qty_ShippedValue != 0{
                qtyValue = qtyValue+GlobalData.sharedInstance.language(key: "shipped")+" :"+customerOrderDetailsModel.customerOrderList[indexPath.row].qty_Shipped+"\n"
            }
            cell.qtyValue.text = qtyValue
            
            var optionDict = customerOrderDetailsModel.customerOrderList[indexPath.row].options
            var stringData = ""
            var finalData : String = ""
            
            if (optionDict.count) > 0 {
                
                for j in 0..<(optionDict.count){
                    var dict = optionDict[j]
                    stringData  = stringData+(dict["label"].stringValue)+": "+(dict["value"].stringValue)+"\n"
                }
                
                finalData = "options".localized+"\n"+stringData
            }
            
            cell.productValue.text = finalData != "" ? finalData.substring(to: finalData.index(before: finalData.endIndex)) : ""
            
            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 6{
            let cell:DeliveryBoyShipmentdetailsCell = tableView.dequeueReusableCell(withIdentifier: "DeliveryBoyShipmentdetailsCell") as! DeliveryBoyShipmentdetailsCell
            cell.name.text = customerOrderDetailsModel.customerOrderDetailsModel.name
            cell.otpValue.text = customerOrderDetailsModel.customerOrderDetailsModel.otp
            cell.mobileNumber.text = customerOrderDetailsModel.customerOrderDetailsModel.mobile
            cell.vehicleNumber.text = customerOrderDetailsModel.customerOrderDetailsModel.vehicleNumber
            GlobalData.sharedInstance.getImageFromUrl(imageUrl:customerOrderDetailsModel.customerOrderDetailsModel.avtar , imageView: cell.profileImage)
            cell.trackButton.addTarget(self, action: #selector(getTrackResult(sender:)), for: .touchUpInside)
            cell.chatWithAdminBtn.addTarget(self, action: #selector(chatWithAdminBtnClick(sender:)), for: .touchUpInside)
            
            cell.selectionStyle = .none
            return cell
        }else {
            let cell:OrderSummaryCell = tableView.dequeueReusableCell(withIdentifier: "OrderSummaryCell") as! OrderSummaryCell
            var Y:CGFloat = 0
            
            for subViews: UIView in cell.dynamicView.subviews {
                subViews.removeFromSuperview()
            }
            
            for i in 0..<customerOrderDetailsModel.customerTotalData.count{
                let optionLabel = UILabel(frame: CGRect(x: CGFloat(5), y: CGFloat(Y), width: CGFloat(SCREEN_WIDTH - 30), height: CGFloat(20)))
                optionLabel.textColor = UIColor.black
                optionLabel.font = UIFont(name: BOLDFONT, size: CGFloat(16))!
                optionLabel.text = customerOrderDetailsModel.customerTotalData[i].label
                cell.dynamicView.addSubview(optionLabel)
                
                Y += 20
                
                let optionValue = UILabel(frame: CGRect(x: CGFloat(5), y: CGFloat(Y), width: CGFloat(SCREEN_WIDTH - 30), height: CGFloat(20)))
                optionValue.textColor = UIColor.darkGray
                optionValue.font = UIFont(name: REGULARFONT, size: CGFloat(15))!
                optionValue.text = customerOrderDetailsModel.customerTotalData[i].value
                cell.dynamicView.addSubview(optionValue)
                
                Y += 20
            }
            
            cell.dynamicViewHeightConstarints.constant = Y
            dynamicSummaryHeight = Y
            
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section == 0{
            return "#"+self.incrementId
        }
        else if(section == 1){
            if customerOrderDetailsModel.customerOrderDetailsModel.billingAddress != ""{
                return "  "+GlobalData.sharedInstance.language(key:"billingaddress")
            }else{
                return ""
            }
        }else if(section == 2){
            if customerOrderDetailsModel.customerOrderDetailsModel.paymentMethod != ""{
                return "  "+GlobalData.sharedInstance.language(key:"billingmethod")
            }else{
                return ""
            }
        }else if(section == 3){
            if customerOrderDetailsModel.customerOrderDetailsModel.shippingAddress != ""{
                return "  "+GlobalData.sharedInstance.language(key:"shippingaddress")
            }else{
                return ""
            }
        }else if(section == 4){
            if customerOrderDetailsModel.customerOrderDetailsModel.shippingMethod != ""{
                return "  "+GlobalData.sharedInstance.language(key:"shipmentmethod")
            }else{
                return ""
            }
        }else if(section == 5){
            return "  "+GlobalData.sharedInstance.language(key:"itemlist")
        }else if(section == 6){
            if customerOrderDetailsModel.customerOrderDetailsModel.isEligibleForDeliveryBoy == 1{
                return "  "+GlobalData.sharedInstance.language(key:"deliveryboydetails")
            }else{
                return ""
            }
        }else if(section == 7){
            return "  "+GlobalData.sharedInstance.language(key:"ordersummary")
        }else{
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 1)    {
            if customerOrderDetailsModel.customerOrderDetailsModel.billingAddress != "" {
                return 30
            }else{
                return CGFloat.leastNonzeroMagnitude
            }
        }else if(section == 2){
            if customerOrderDetailsModel.customerOrderDetailsModel.paymentMethod != ""  {
                return 30
            }else{
                return CGFloat.leastNonzeroMagnitude
            }
        }else if(section == 3)  {
            if customerOrderDetailsModel.customerOrderDetailsModel.shippingAddress != ""    {
                return 30
            }else{
                return CGFloat.leastNonzeroMagnitude
            }
        }else if(section == 4)  {
            if customerOrderDetailsModel.customerOrderDetailsModel.shippingMethod != "" {
                return 30
            }else{
                return CGFloat.leastNonzeroMagnitude
            }
        }else if section == 5   {
            return 30
        }else    {
            return 30
        }
    }
    
    @objc func getTrackResult(sender: UIButton){
        callingExtraHttpi()
    }
    
    //MARK:- Calling API
    func callingExtraHttpi(){
        GlobalData.sharedInstance.showLoader()
        
        var requstParams = [String:Any]()
        requstParams["deliveryboyId"] = customerOrderDetailsModel.customerOrderDetailsModel.deliveyBoyID
        GlobalData.sharedInstance.callingHttpRequest(params:requstParams, apiname:"expressdelivery/api/GetDeliveryboyLocation", currentView: self){success,responseObject in
            if success == 1{
                let dict = JSON(responseObject as! NSDictionary)
                print(responseObject)
                GlobalData.sharedInstance.dismissLoader()
                if dict["success"].intValue == 1{
                    let lat = dict["latitude"].stringValue
                    let lon = dict["longitude"].stringValue
                    let url = "http://maps.apple.com/maps?saddr=\(self.currentLocation.coordinate.latitude),\(self.currentLocation.coordinate.longitude)&daddr=\(lat),\(lon)"
                    UIApplication.shared.openURL(URL(string:url)!)
                }
            }else if success == 2{
                self.callingHttppApi()
                GlobalData.sharedInstance.dismissLoader()
            }
        }
    }
    
    @objc func chatWithAdminBtnClick(sender: UIButton){
        whichApiToProcess = "updateTokenToDataBase"
        callingHttppApi()
    }
}
