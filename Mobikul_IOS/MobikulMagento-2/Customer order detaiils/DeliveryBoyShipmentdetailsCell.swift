//
//  DeliveryBoyShipmentdetailsCell.swift
//  GoFast
//
//  Created by kunal on 10/04/18.
//  Copyright © 2018 kunal. All rights reserved.
//

import UIKit

class DeliveryBoyShipmentdetailsCell: UITableViewCell {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var otpLabel: UILabel!
    @IBOutlet weak var otpValue: UILabel!
    @IBOutlet weak var mobileNumber: UILabel!
    @IBOutlet weak var vehicleNumber: UILabel!
    
    @IBOutlet weak var trackButton: UIButton!
    @IBOutlet weak var chatWithAdminBtn: UIButton!
    
    @IBOutlet weak var btnSuperView : UIView!
    @IBOutlet weak var line : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //text
        otpLabel.text = GlobalData.sharedInstance.language(key: "otp")
        trackButton.setTitle(GlobalData.sharedInstance.language(key: "track"), for: .normal)
        chatWithAdminBtn.setTitle(GlobalData.sharedInstance.language(key: "chatwithadmin"), for: .normal)
        //colors
        trackButton.setTitleColor(UIColor().HexToColor(hexString: BUTTON_COLOR), for: .normal)
        chatWithAdminBtn.setTitleColor(UIColor().HexToColor(hexString: BUTTON_COLOR), for: .normal)
        otpValue.textColor = UIColor().HexToColor(hexString: REDCOLOR)
        line.backgroundColor = UIColor().HexToColor(hexString: BUTTON_COLOR)
        //Button view
        btnSuperView.layer.cornerRadius = 5.0
        btnSuperView.layer.borderColor = UIColor().HexToColor(hexString: BUTTON_COLOR).cgColor
        btnSuperView.layer.borderWidth = 2.0
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
