//

/*
 JaguarImport
 @Category Webkul
 @author Webkul
 Created by: Webkul on 16/07/18
 FileName: ContactUSController.swift
 Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 @license https://store.webkul.com/license.html
 */


import UIKit
import MessageUI


class ContactUSController: UIViewController {
    
  
    
    @IBOutlet weak var contactUsInfo: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var nameField: UITextField!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var telephonrLabel: UILabel!
    @IBOutlet var telephoneField: UITextField!
    @IBOutlet var messageLabel: UILabel!
    @IBOutlet var messageField: UITextView!
    @IBOutlet var sendButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = "name".localized
        emailLabel.text = "email".localized
        telephonrLabel.text = "telephoneno".localized
        messageLabel.text = "message".localized
        contactUsInfo.text = "contactusinfo".localized
        sendButton.setTitle("send".localized, for: .normal)
        sendButton.setTitleColor(UIColor.white, for: .normal)
        sendButton.layer.cornerRadius = 5;
        sendButton.layer.masksToBounds = true
        sendButton.backgroundColor = UIColor().HexToColor(hexString: BUTTON_COLOR)
      
        self.navigationController?.isNavigationBarHidden = false
        
        self.navigationItem.title = "contactus".localized
        
        if defaults.object(forKey: "customerEmail") != nil  {
            emailField.text = defaults.object(forKey: "customerEmail") as? String
        }
        
        if defaults.object(forKey: "customerName") != nil  {
            nameField.text = defaults.object(forKey: "customerName") as? String
        }
    }
    
    //MARK:- API Call
    func callingHttppApi(){
        GlobalData.sharedInstance.showLoader()
        
        var requstParams = [String:Any]()
        requstParams["storeId"] = defaults.object(forKey:"storeId") as! String
        requstParams["name"] = nameField.text
        requstParams["email"] = emailField.text
        requstParams["telephone"] = telephoneField.text
        requstParams["comment"] = messageField.text
        
        GlobalData.sharedInstance.callingHttpRequest(params:requstParams, apiname:"mobikulhttp/contact/post", currentView: self){success,responseObject in
            if success == 1{
                
                GlobalData.sharedInstance.dismissLoader()
                
                let dict = responseObject as! NSDictionary
                
                print(dict)
                if dict.object(forKey: "success") as! Bool == true{
                    
                    let  AC = UIAlertController(title: GlobalData.sharedInstance.language(key: "success"), message: dict.object(forKey: "message") as? String, preferredStyle: .alert)
                    let okBtn = UIAlertAction(title: GlobalData.sharedInstance.language(key: "ok"), style:.default, handler: {(_ action: UIAlertAction) -> Void in
                        self.dismiss(animated: true, completion: nil)
                    })
                    
                    AC.addAction(okBtn)
                    self.present(AC, animated: true, completion: {  })
                }else{
                    GlobalData.sharedInstance.showErrorSnackBar(msg: dict.object(forKey: "message") as! String)
                }
            }else if success == 2{
                GlobalData.sharedInstance.dismissLoader()
                self.callingHttppApi()
            }
        }
    }
    
   
    @IBAction func sendClick(_ sender: UIButton) {
        var errorMessage = "pleasefill".localized + " "
        var isValid:Int = 1
        
        if nameField.text == ""{
            errorMessage += "name".localized
            isValid = 0
        }else if emailField.text == ""{
            errorMessage += "email".localized
            isValid = 0
        }else if telephoneField.text == ""{
            errorMessage += "telephoneno".localized
            isValid = 0
        }else if messageField.text == ""{
            errorMessage += "message".localized
            isValid = 0
        }
        
        if isValid == 0{
            GlobalData.sharedInstance.showWarningSnackBar(msg: errorMessage)
        }else{
            //call API
            callingHttppApi()
        }
    }
}

// MARK: - UITextFieldDelegate
extension ContactUSController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        if textField == telephoneField {
            let ACCEPTABLE_CHARACTERS = "0123456789+"
            
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
        }
        return true
    }
}
