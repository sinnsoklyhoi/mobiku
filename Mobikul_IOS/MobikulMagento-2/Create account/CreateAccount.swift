//
//  CreateAccount.swift
//  Magento2MobikulNew
//
//  Created by Webkul  on 18/08/17.
//  Copyright © 2017 Webkul . All rights reserved.
//

import UIKit
import MaterialComponents

class CreateAccount: UIViewController ,UIPickerViewDelegate, UIPickerViewDataSource{
    
    let defaults = UserDefaults.standard
    var createAccountModel:CreateAccountModel!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var prefixTextField: UIFloatLabelTextField!
    @IBOutlet weak var firstNametextField: UIFloatLabelTextField!
    @IBOutlet weak var middleNametextField: UIFloatLabelTextField!
    @IBOutlet weak var lastNametextField: UIFloatLabelTextField!
    @IBOutlet weak var suffixtextfield: UIFloatLabelTextField!
    @IBOutlet weak var emailTextFeild: UIFloatLabelTextField!
    @IBOutlet weak var mobileNumbertextfield: UIFloatLabelTextField!
    @IBOutlet weak var dateOfBirthTextfield: UIFloatLabelTextField!
    @IBOutlet weak var taxvalueTextField: UIFloatLabelTextField!
    @IBOutlet weak var genderTextField: UIFloatLabelTextField!
    @IBOutlet weak var passwordtextField: HideShowPasswordTextField!
    @IBOutlet weak var confirmTextField: HideShowPasswordTextField!
    @IBOutlet weak var passwordHint: UILabel!
    
    @IBOutlet weak var prefixTextfieldHeightConstarints: NSLayoutConstraint!
    @IBOutlet weak var middleNameheightConstraints: NSLayoutConstraint!
    @IBOutlet weak var suffixtextFieldHeightConstarints: NSLayoutConstraint!
    @IBOutlet weak var mobileNumberHeightConstarints: NSLayoutConstraint!
    @IBOutlet weak var dateOfBirthTextHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var taxValueHeightConstarints: NSLayoutConstraint!
    @IBOutlet weak var genderheightConstarints: NSLayoutConstraint!
    @IBOutlet weak var registerButton: UIButton!
    
    //GDPR
    @IBOutlet weak var gdprTxtView: UITextView!
    @IBOutlet weak var checkBoxBtn : UIButton!
    
    var prefixValueArray:NSArray = []
    var suffixValueArray:NSArray = []
    var genderValueArray:NSMutableArray = []
    var keyBoardFlag:Int = 1
    var whichApitoprocess:String = ""
    var movetoSignal:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        whichApitoprocess = ""
        callingHttppApi()
        self.navigationItem.title = GlobalData.sharedInstance.language(key: "createaccount")
        
        prefixTextField.placeholder = GlobalData.sharedInstance.language(key: "prefix")
        firstNametextField.placeholder = GlobalData.sharedInstance.language(key: "firstname")
        lastNametextField.placeholder = GlobalData.sharedInstance.language(key: "lastname")
        middleNametextField.placeholder = GlobalData.sharedInstance.language(key: "middlename")
        suffixtextfield.placeholder = GlobalData.sharedInstance.language(key: "suffix")
        emailTextFeild.placeholder = GlobalData.sharedInstance.language(key: "email")
        mobileNumbertextfield.placeholder = GlobalData.sharedInstance.language(key: "mobileno")
        dateOfBirthTextfield.placeholder = GlobalData.sharedInstance.language(key: "dob")
        taxvalueTextField.placeholder = GlobalData.sharedInstance.language(key: "taxvat")
        genderTextField.placeholder = GlobalData.sharedInstance.language(key: "gender")
        passwordtextField.placeholder = GlobalData.sharedInstance.language(key: "password")
        confirmTextField.placeholder = GlobalData.sharedInstance.language(key: "confirmpassword")
        
        registerButton.setTitle(GlobalData.sharedInstance.language(key: "signup"), for: .normal)
        registerButton.setButtonLayout(radii: 5.0, color: BUTTON_COLOR)
        
        passwordHint.text = "passwordhint".localized
        prefixTextField.textAlignment = UITextField().isLanguageLayoutDirectionRightToLeft() ? .right : .left
        firstNametextField.textAlignment = UITextField().isLanguageLayoutDirectionRightToLeft() ? .right : .left
        lastNametextField.textAlignment = UITextField().isLanguageLayoutDirectionRightToLeft() ? .right : .left
        middleNametextField.textAlignment = UITextField().isLanguageLayoutDirectionRightToLeft() ? .right : .left
        suffixtextfield.textAlignment = UITextField().isLanguageLayoutDirectionRightToLeft() ? .right : .left
        emailTextFeild.textAlignment = UITextField().isLanguageLayoutDirectionRightToLeft() ? .right : .left
        mobileNumbertextfield.textAlignment = UITextField().isLanguageLayoutDirectionRightToLeft() ? .right : .left
        dateOfBirthTextfield.textAlignment = UITextField().isLanguageLayoutDirectionRightToLeft() ? .right : .left
        taxvalueTextField.textAlignment = UITextField().isLanguageLayoutDirectionRightToLeft() ? .right : .left
        genderTextField.textAlignment = UITextField().isLanguageLayoutDirectionRightToLeft() ? .right : .left
        passwordtextField.textAlignment = UITextField().isLanguageLayoutDirectionRightToLeft() ? .right : .left
        confirmTextField.textAlignment = UITextField().isLanguageLayoutDirectionRightToLeft() ? .right : .left
        self.mainView.isHidden = true
        
        checkBoxBtn.layer.borderColor = UIColor.black.cgColor
        checkBoxBtn.layer.borderWidth = 1.0
        checkBoxBtn.setImage(nil, for: .normal)
        
        prefixTextField.floatLabelActiveColor = UIColor().HexToColor(hexString: BUTTON_COLOR, alpha: 1.0)
        firstNametextField.floatLabelActiveColor = UIColor().HexToColor(hexString: BUTTON_COLOR, alpha: 1.0)
        middleNametextField.floatLabelActiveColor = UIColor().HexToColor(hexString: BUTTON_COLOR, alpha: 1.0)
        lastNametextField.floatLabelActiveColor = UIColor().HexToColor(hexString: BUTTON_COLOR, alpha: 1.0)
        suffixtextfield.floatLabelActiveColor = UIColor().HexToColor(hexString: BUTTON_COLOR, alpha: 1.0)
        emailTextFeild.floatLabelActiveColor = UIColor().HexToColor(hexString: BUTTON_COLOR, alpha: 1.0)
        mobileNumbertextfield.floatLabelActiveColor = UIColor().HexToColor(hexString: BUTTON_COLOR, alpha: 1.0)
        dateOfBirthTextfield.floatLabelActiveColor = UIColor().HexToColor(hexString: BUTTON_COLOR, alpha: 1.0)
        taxvalueTextField.floatLabelActiveColor = UIColor().HexToColor(hexString: BUTTON_COLOR, alpha: 1.0)
        genderTextField.floatLabelActiveColor = UIColor().HexToColor(hexString: BUTTON_COLOR, alpha: 1.0)
    }
    
    func donePressed(_ sender: UIBarButtonItem) {
        dateOfBirthTextfield.resignFirstResponder()
        prefixTextField.resignFirstResponder()
        suffixtextfield.resignFirstResponder()
        genderTextField.resignFirstResponder()
    }
    
    func callingHttppApi(){
        
        GlobalData.sharedInstance.showLoader()
        self.view.isUserInteractionEnabled = false
        if whichApitoprocess == "createaccount"{
            
            var requstParams = [String:Any]()
            let quoteId = defaults.object(forKey:"quoteId")
            let deviceToken = defaults.object(forKey:"deviceToken")
            requstParams["storeId"] = defaults.object(forKey:"storeId") as! String
            requstParams["websiteId"] = DEFAULT_WEBSITE_ID
            if quoteId != nil{
                requstParams["quoteId"] = quoteId
            }
            if deviceToken != nil{
                requstParams["token"] = deviceToken
            }
            requstParams["firstName"] = firstNametextField.text
            requstParams["lastName"] = lastNametextField.text
            requstParams["email"] = emailTextFeild.text
            requstParams["prefix"] = prefixTextField.text
            requstParams["suffix"] = suffixtextfield.text
            requstParams["middleName"] = middleNametextField.text
            requstParams["mobile"] = mobileNumbertextfield.text
            requstParams["dob"] = dateOfBirthTextfield.text
            requstParams["taxvat"] = taxvalueTextField.text
            requstParams["isSocial"] = "0"
            requstParams["pictureURL"] = ""
            requstParams["password"] = passwordtextField.text
            var value:String = ""
            if genderTextField.text == "Male"{
                value = "1"
            }else if genderTextField.text == "Female"{
                value = "0"
            }
            requstParams["gender"] = value
            GlobalData.sharedInstance.callingHttpRequest(params:requstParams, apiname:"mobikulhttp/customer/createAccount", currentView: self){success,responseObject in
                if success == 1{
                    
                    GlobalData.sharedInstance.dismissLoader()
                    self.view.isUserInteractionEnabled = true
                    let dict = responseObject as! NSDictionary
                    if dict.object(forKey: "success") as! Bool == true{
                        self.defaults.set(dict.object(forKey: "customerEmail"), forKey: "customerEmail")
                        self.defaults.set(dict.object(forKey: "customerToken"), forKey: "customerId")
                        self.defaults.set(dict.object(forKey: "customerName"), forKey: "customerName")
                        if(self.defaults.object(forKey: "quoteId") != nil){
                            self.defaults.set(nil, forKey: "quoteId")
                            self.defaults.synchronize()
                        }
                        UserDefaults.standard.removeObject(forKey: "quoteId")
                        self.defaults.synchronize()
                        GlobalData.sharedInstance.showSuccessSnackBar(msg: dict.object(forKey: "message") as! String)
                        if self.movetoSignal == "cart"{
                            self.performSegue(withIdentifier: "checkout", sender: self)
                        }
                        self.tabBarController?.tabBar.isHidden = false
                        self.navigationController?.popToRootViewController(animated:true)
                    } else {
                        GlobalData.sharedInstance.showErrorSnackBar(msg: dict.object(forKey: "message") as! String)
                    }
                } else if success == 2 {
                    GlobalData.sharedInstance.dismissLoader()
                    self.callingHttppApi()
                }
            }
        } else {
            var requstParams = [String:Any]()
            requstParams["storeId"] = defaults.object(forKey:"storeId") as! String
            GlobalData.sharedInstance.callingHttpRequest(params:requstParams, apiname:"mobikulhttp/customer/createAccountFormData", currentView: self){success,responseObject in
                if success == 1{
                    
                    print(responseObject)
                    self.createAccountModel = CreateAccountModel(data:JSON(responseObject as! NSDictionary))
                    self.doFurtherProcessingwithResult()
                    self.view.isUserInteractionEnabled = true
                    
                }else if success == 2{
                    GlobalData.sharedInstance.dismissLoader()
                    self.callingHttppApi()
                }
            }
        }
    }
    
    @IBAction func dismissKeyboard(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func prefixClick(_ sender: Any) {
        if createAccountModel.isPrefixHasOption == true{
            let thePicker = UIPickerView()
            thePicker.tag = 1000
            prefixTextField.inputView = thePicker
            thePicker.delegate = self
        }
    }
    
    @IBAction func suffixClick(_ sender: UIFloatLabelTextField) {
        if createAccountModel.isPrefixHasOption == true{
            let thePicker = UIPickerView()
            thePicker.tag = 2000
            suffixtextfield.inputView = thePicker
            thePicker.delegate = self
        }
    }
    
    @IBAction func dateOfBirthClick(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.tag = 4000
        datePickerView.datePickerMode = UIDatePickerMode.date
        dateOfBirthTextfield.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(CreateAccount.datePickerFromValueChanged), for: UIControlEvents.valueChanged)
    }
    
    @IBAction func genderClick(_ sender: UIFloatLabelTextField) {
        let thePicker = UIPickerView()
        thePicker.tag = 3000
        genderTextField.inputView = thePicker
        thePicker.delegate = self
    }
    
    @objc func datePickerFromValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = createAccountModel.dateFormat
        dateOfBirthTextfield.text = dateFormatter.string(from: sender.date)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView.tag == 1000){
            return self.prefixValueArray.count
        }else if(pickerView.tag == 2000){
            return self.suffixValueArray.count
        }else if(pickerView.tag == 3000){
            return self.genderValueArray.count
        }else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1000{
            return prefixValueArray.object(at: row) as? String
        }else if pickerView.tag == 2000{
            return suffixValueArray.object(at: row) as? String
        }else if pickerView.tag == 3000{
            return genderValueArray.object(at: row) as? String
        }else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        if(pickerView.tag == 1000){
            prefixTextField.text = prefixValueArray.object(at: row) as? String
        }else if(pickerView.tag == 2000){
            suffixtextfield.text = suffixValueArray.object(at: row) as? String
        }else if(pickerView.tag == 3000){
            genderTextField.text = genderValueArray.object(at: row) as? String
        }
    }
    
    func doFurtherProcessingwithResult(){
        self.mainView.isHidden = false
        GlobalData.sharedInstance.dismissLoader()
        
        prefixValueArray = createAccountModel.prefixValue! as NSArray
        suffixValueArray = createAccountModel.suffixValue! as NSArray
        if createAccountModel.isGenderRequired{
            self.genderValueArray = ["Female", "Male"]
        }else{
            self.genderValueArray = ["Female", "Male",""]
        }
        
//        self.mainViewHeightConstarints.constant -= 350
        var totalHeight:CGFloat = 0
        self.prefixTextfieldHeightConstarints.constant = 0
        self.middleNameheightConstraints.constant = 0
        self.suffixtextFieldHeightConstarints.constant = 0
        self.mobileNumberHeightConstarints.constant = 0
        self.dateOfBirthTextHeightConstraints.constant = 0
        self.taxValueHeightConstarints.constant = 0
        self.genderheightConstarints.constant = 0
        
        self.prefixTextField.isHidden = true
        self.middleNametextField.isHidden = true
        self.suffixtextfield.isHidden = true
        self.mobileNumbertextfield.isHidden = true
        self.dateOfBirthTextfield.isHidden = true
        self.taxvalueTextField.isHidden = true
        self.genderTextField.isHidden = true
        
        print("adad",createAccountModel.isPrefixVisible)
        
        if createAccountModel.isPrefixVisible{
            self.prefixTextField.isHidden = false
            totalHeight += 50
            self.prefixTextfieldHeightConstarints.constant = 50
        }
        if createAccountModel.isMiddleNameVisible{
            self.middleNametextField.isHidden = false
            totalHeight += 50
            self.middleNameheightConstraints.constant = 50
        }
        if createAccountModel.isSuffixVisible{
            self.suffixtextfield.isHidden = false
            totalHeight += 50
            self.suffixtextFieldHeightConstarints.constant = 50
        }
        if createAccountModel.isMobileNumberVisible{
            self.mobileNumbertextfield.isHidden = false
            totalHeight += 50
            self.mobileNumberHeightConstarints.constant = 50
        }
        if createAccountModel.isDobVisible{
            self.dateOfBirthTextfield.isHidden = false
            totalHeight += 50
            self.dateOfBirthTextHeightConstraints.constant = 50
        }
        if createAccountModel.isTaxVisible{
            self.taxvalueTextField.isHidden = false
            totalHeight += 50
            self.taxValueHeightConstarints.constant = 50
        }
        if createAccountModel.isGenderVisible{
            self.genderTextField.isHidden = false
            totalHeight += 50
            self.genderheightConstarints.constant = 50
        }
        
        //GDPR setup
        callGDPR()
    }
    
    @IBAction func registerClick(_ sender: Any) {
        
        if (createAccountModel.gdprEnable && createAccountModel.tncAccountEnable && checkBoxBtn.isSelected) || !createAccountModel.gdprEnable {
            //GDPR disabled, GDPR enabled and selected
            var errorMessage = ""
            var isValid:Int = 1
            print("calling")
            if firstNametextField.text == ""{
                isValid = 0
                errorMessage = GlobalData.sharedInstance.language(key: "pleasefillfirstname")
            }else if lastNametextField.text == ""{
                isValid = 0
                errorMessage = GlobalData.sharedInstance.language(key: "pleasefilllastname")
            }else if !GlobalData.sharedInstance.checkValidEmail(data: emailTextFeild.text!){
                isValid = 0
                errorMessage = GlobalData.sharedInstance.language(key: "pleasefilllvalidemail")
            }else if passwordtextField.text == ""{
                isValid = 0
                errorMessage = GlobalData.sharedInstance.language(key: "pleasefillpassword")
            }else if confirmTextField.text == ""{
                isValid = 0
                errorMessage = GlobalData.sharedInstance.language(key: "pleasefillconfirmnewpassword")
            }
            if createAccountModel.isPrefixRequired{
                if prefixTextField.text == ""{
                    isValid = 0
                    errorMessage = GlobalData.sharedInstance.language(key: "plesefillprefix")
                }
            }
            if createAccountModel.isSuffixRequired{
                if suffixtextfield.text == ""{
                    isValid = 0
                    errorMessage = GlobalData.sharedInstance.language(key: "plesefillsuffix")
                }
            }
            if createAccountModel.isMobileNumberRequired{
                if mobileNumbertextfield.text == ""{
                    isValid = 0
                    errorMessage = GlobalData.sharedInstance.language(key: "pleasefillmobilenumber")
                }
            }
            if createAccountModel.isDobRequired{
                if dateOfBirthTextfield.text == ""{
                    isValid = 0
                    errorMessage = GlobalData.sharedInstance.language(key: "plesefilldob")
                }
            }
            if createAccountModel.isTaxRequired{
                if taxvalueTextField.text == ""{
                    isValid = 0
                    errorMessage = GlobalData.sharedInstance.language(key: "plesefilltaxvat")
                }
            }
            if createAccountModel.isGenderRequired{
                if genderTextField.text == ""{
                    isValid = 0
                    errorMessage = GlobalData.sharedInstance.language(key: "plesefillgender")
                }
            }
            
            if isValid == 0 {
                GlobalData.sharedInstance.showErrorSnackBar(msg: errorMessage)
            } else {
                if passwordtextField.text != confirmTextField.text{
                    GlobalData.sharedInstance.showErrorSnackBar(msg: GlobalData.sharedInstance.language(key: "passwordnotmatch"))
                }else if (passwordtextField.text?.count)! < createAccountModel.passwordLength {
                    GlobalData.sharedInstance.showErrorSnackBar(msg: "passwordlength".localized + " " + "\(createAccountModel.passwordLength)")
                }else{
                    whichApitoprocess = "createaccount"
                    callingHttppApi()
                }
            }
        }else{
            GlobalData.sharedInstance.showErrorSnackBar(msg: "pleaseagreetogdprtermsandconditions".localized)
        }
    }
}

//MARK:- GDPR
extension CreateAccount : UITextViewDelegate {
    func callGDPR() {
        if createAccountModel.gdprEnable && createAccountModel.tncAccountEnable {
            //GDPR terms and conditions button
            let firstPart = "ihavereadandagreeto".localized
            let tnc = "termsandconditions".localized
            let secondPart = "ofthewebsite".localized
            let range = ("\(firstPart)\(tnc)\(secondPart)" as NSString).range(of: "\(firstPart)\(tnc)\(secondPart)")
            
            // You must set the formatting of the link manually
            let linkAttributes: [NSAttributedStringKey: Any] = [
                .link: NSURL(string: "")!,
                .foregroundColor: UIColor.blue
            ]
            
            let attributedString = NSMutableAttributedString(string: "\(firstPart)\(tnc)\(secondPart)")
            // Set the 'click here' substring to be the link
            attributedString.setAttributes(linkAttributes, range: NSRange(location: firstPart.count, length: tnc.count))
            //            attributedString.addAttribute(.link, value: tnc, range: NSRange(location: firstPart.count, length: tnc.count))
            attributedString.addAttribute(.foregroundColor, value: UIColor.black , range: range)
            
            gdprTxtView.attributedText = attributedString
        } else {
            gdprTxtView.isHidden = true
            checkBoxBtn.isHidden = true
        }
    }
    
    @available(iOS 10.0, *)
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        //GDPR : Terms and Conditions
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "GDPRWebViewController") as! GDPRWebViewController
        vc.backImg = takeScreenshot()
        vc.content = createAccountModel.tncAccountContent
        self.navigationController?.present(vc, animated: true, completion: nil)
        
        return true
    }
    
    /// Takes the screenshot of the screen and returns the corresponding image
    ///
    /// - Parameter shouldSave: Boolean flag asking if the image needs to be saved to user's photo library. Default set to 'true'
    /// - Returns: (Optional)image captured as a screenshot
    open func takeScreenshot() -> UIImage? {
        self.tabBarController?.tabBar.isHidden = true
        var screenshotImage :UIImage?
        let layer = UIApplication.shared.keyWindow!.layer
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else {return nil}
        layer.render(in:context)
        screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.tabBarController?.tabBar.isHidden = false
        return screenshotImage
    }
    
    @IBAction func checkBtnClicked(_ sender: UIButton)  {
        if sender.isSelected    {
            checkBoxBtn.setImage(nil, for: .normal)
            sender.isSelected = false
        }else{
            sender.isSelected = true
        }
    }
}
