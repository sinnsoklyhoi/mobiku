//
//  ProductDescription.swift
//  Magento2MobikulNew
//
//  Created by Webkul  on 14/09/17.
//  Copyright © 2017 Webkul . All rights reserved.
//

import UIKit

class ProductDescription: UIViewController {
    
    @IBOutlet weak var descriptionWebView: UIWebView!
    var descriptionData:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.title = GlobalData.sharedInstance.language(key: "description")
        self.descriptionWebView.loadHTMLString(descriptionData, baseURL: nil)
    }
}
