//
/**
MobikulMagento-2
@Category Webkul
@author Webkul <support@webkul.com>
FileName: OrderReturnsFieldTableViewCell.swift
Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
@license https://store.webkul.com/license.html ASL Licence
@link https://store.webkul.com/license.html

*/


import UIKit

class OrderReturnsFieldTableViewCell: UITableViewCell {

    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var value: UITextField!
    
    static var identifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    var rowType: OrdersReturnRows? {
        didSet {
            if rowType == .FindOrderBy {
                heading.text = "findorderby".localized
                let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
                imgView.image = UIImage(named: "ic_down")
                value.rightView = imgView
                value.rightViewMode = .always
            } else if rowType == .OrderId {
                heading.text = "orderidreturn".localized
                value.rightViewMode = .never
            } else if rowType == .Email {
                heading.text = "emailreturn".localized
                value.rightViewMode = .never
            } else {
                heading.text = "billinglastname".localized
                value.rightViewMode = .never
            }
        }
    }
    
    var data: OrdersReturnsDataModel? {
        didSet {
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func updateVaueChanged(_ textField: UITextField) {
        if rowType == .FindOrderBy {
            
        } else if rowType == .OrderId {
            
        } else if rowType == .Email {
            
        } else {
            
        }
    }
}
