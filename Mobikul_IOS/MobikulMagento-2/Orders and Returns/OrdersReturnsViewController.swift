//
/**
MobikulMagento-2
@Category Webkul
@author Webkul <support@webkul.com>
FileName: OrdersReturnsViewController.swift
Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
@license https://store.webkul.com/license.html ASL Licence
@link https://store.webkul.com/license.html

*/


import UIKit

class OrdersReturnsViewController: UIViewController {
    
    @IBOutlet weak var orderReturnTblView: UITableView!
    var ordersReturnsViewModel = OrdersReturnsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        orderReturnTblView.register(OrderReturnsFieldTableViewCell.nib, forCellReuseIdentifier: OrderReturnsFieldTableViewCell.identifier)
        orderReturnTblView.rowHeight = UITableViewAutomaticDimension
        orderReturnTblView.estimatedRowHeight = 200
        
        orderReturnTblView.delegate = ordersReturnsViewModel
        orderReturnTblView.dataSource = ordersReturnsViewModel
    }
}
