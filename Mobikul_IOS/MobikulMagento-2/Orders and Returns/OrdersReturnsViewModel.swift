//
/**
MobikulMagento-2
@Category Webkul
@author Webkul <support@webkul.com>
FileName: OrdersReturnsViewModel.swift
Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
@license https://store.webkul.com/license.html ASL Licence
@link https://store.webkul.com/license.html

*/


import Foundation

class OrdersReturnsViewModel: NSObject {
    
    var ordersReturnsDataModel = OrdersReturnsDataModel()
    
}

extension OrdersReturnsViewModel: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: OrderReturnsFieldTableViewCell.identifier, for: indexPath) as! OrderReturnsFieldTableViewCell
        cell.value.tag = indexPath.row
        cell.data = ordersReturnsDataModel
        
        switch indexPath.row {
        case 0:
            cell.rowType = .OrderId
            
        case 1:
            cell.rowType = .BillingLastName
            
        case 2:
            cell.rowType = .FindOrderBy
            
        case 3:
            cell.rowType = .Email
            
        default:
            print("wrong index path")
        }
        
        return UITableViewCell()
    }
}

// MARK: - Data Model
struct OrdersReturnsDataModel {
    
    var orderId: String = ""
    var billingLastName: String = ""
    var findOrderBy: String = "" // 1: email , 2: Zip
    var emailOrZip: String = ""
    
    init() {
    }
    
    mutating func updateValueForField(rowType: OrdersReturnRows, value: String) {
        if rowType == .OrderId {
            orderId = value
        } else if rowType == .BillingLastName {
            billingLastName = value
        } else if rowType == .FindOrderBy {
            findOrderBy = value
        } else {
            emailOrZip = value
        }
    }
}

// MARK: - Enums

enum OrdersReturnRows: String {
    case OrderId
    case BillingLastName
    case FindOrderBy
    case Email
}
