//
//  MyproductreviewTableViewCell.swift
//  Magento2MobikulNew
//
//  Created by Webkul  on 23/08/17.
//  Copyright © 2017 Webkul . All rights reserved.
//

import UIKit

class MyproductreviewTableViewCell: UITableViewCell {
    
    
    
    @IBOutlet weak var imageData: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var ratingView: HCSStarRatingView!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var viewButton: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        
    }
    
    override func layoutSubviews() {
        viewButton.setTitle(GlobalData.sharedInstance.language(key: "view"), for: .normal)
        viewButton.backgroundColor = UIColor().HexToColor(hexString: BUTTON_COLOR)
        viewButton.layer.cornerRadius = 5
        viewButton.clipsToBounds = true
        ratingView.tintColor = UIColor().HexToColor(hexString: STAR_COLOR)
        
        
        
        
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    
}
