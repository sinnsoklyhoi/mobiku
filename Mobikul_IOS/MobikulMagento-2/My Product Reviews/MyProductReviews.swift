//
//  MyProductReviews.swift
//  DummySwift
//
//  Created by Webkul on 22/11/16.
//  Copyright © 2016 Webkul. All rights reserved.
//

import UIKit

class MyProductReviews: UIViewController,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var productReviewsTableView: UITableView!
    @IBOutlet weak var emptylbl : UILabel!
    
    var whichApiDataToprocess: String = ""
    var pageNumber:Int = 0
    var loadPageRequestFlag: Bool = false
    var indexPathValue:IndexPath!
    var reviewId:String!
    let defaults = UserDefaults.standard
    var myProductReviewViewModel:MyProductReviewViewModel!
    
    let globalObjectMyProductReviews = GlobalData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.title = GlobalData.sharedInstance.language(key: "myproductreview")
        pageNumber = 1
        loadPageRequestFlag = true
        
        productReviewsTableView.separatorStyle = .none
        productReviewsTableView.rowHeight = 200
        productReviewsTableView.estimatedRowHeight = UITableViewAutomaticDimension
        
        callingHttppApi()
        
        emptylbl.isHidden = true
        emptylbl.text = GlobalData.sharedInstance.language(key: "Noproductreviewsavailable")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
    }
    
    func callingHttppApi(){
        
        if pageNumber == 1{
            GlobalData.sharedInstance.showLoader()
        }
        var requstParams = [String:Any]();
        requstParams["storeId"] = defaults.object(forKey:"storeId") as! String
        requstParams["customerToken"] = defaults.object(forKey:"customerId") as! String
        let width = String(format:"%f", SCREEN_WIDTH * UIScreen.main.scale)
        requstParams["width"] = width
        requstParams["pageNumber"] = "1"
        GlobalData.sharedInstance.callingHttpRequest(params:requstParams, apiname:"mobikulhttp/customer/reviewList", currentView: self){success,responseObject in
            if success == 1{
                
                print((responseObject as! NSDictionary))
                self.myProductReviewViewModel = MyProductReviewViewModel(data: JSON(responseObject as! NSDictionary))
                self.doFurtherProcessingWithResult()
                
            }else if success == 2{
                GlobalData.sharedInstance.dismissLoader()
                self.callingHttppApi()
            }
        }
    }
    
    func doFurtherProcessingWithResult(){
        DispatchQueue.main.async {
            GlobalData.sharedInstance.dismissLoader()
            self.view.isUserInteractionEnabled = true
            self.productReviewsTableView.delegate = self
            self.productReviewsTableView.dataSource = self
            self.productReviewsTableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if myProductReviewViewModel.getMyProductReviewData.count > 0    {
            emptylbl.isHidden = true
            return myProductReviewViewModel.getMyProductReviewData.count
        }else{
            emptylbl.isHidden = false
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:MyproductreviewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "reviewcell") as! MyproductreviewTableViewCell
        cell.imageData.image = UIImage(named: "ic_placeholder.png")
        cell.productName.text = myProductReviewViewModel.getMyProductReviewData[indexPath.row].productName
        cell.date.text = myProductReviewViewModel.getMyProductReviewData[indexPath.row].date
        cell.detailsLabel.text = myProductReviewViewModel.getMyProductReviewData[indexPath.row].details
        cell.viewButton.tag = indexPath.row
        
        let viewByTap = UITapGestureRecognizer(target: self, action: #selector(self.viewDetails))
        viewByTap.numberOfTapsRequired = 1
        cell.viewButton.addGestureRecognizer(viewByTap)
        GlobalData.sharedInstance.getImageFromUrl(imageUrl: myProductReviewViewModel.getMyProductReviewData[indexPath.row].imageUrl, imageView: cell.imageData!)
        
        if myProductReviewViewModel.getMyProductReviewData[indexPath.row].ratingData.count > 0{
            let dict = JSON(myProductReviewViewModel.getMyProductReviewData[indexPath.row].ratingData[0])
            cell.ratingView.value = CGFloat(dict["ratingValue"].floatValue / 20)
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SCREEN_WIDTH/3 + 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "catalogproduct") as! CatalogProduct
        vc.productId = myProductReviewViewModel.getMyProductReviewData[indexPath.row].productId
        vc.productName = myProductReviewViewModel.getMyProductReviewData[indexPath.row].productName
        vc.productImageUrl = myProductReviewViewModel.getMyProductReviewData[indexPath.row].imageUrl
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentCellCount = self.productReviewsTableView.numberOfRows(inSection: 0)
        for cell: UITableViewCell in self.productReviewsTableView.visibleCells {
            indexPathValue = self.productReviewsTableView.indexPath(for: cell)!
            if indexPathValue.row == self.productReviewsTableView.numberOfRows(inSection: 0) - 1 {
                if (myProductReviewViewModel.totalCout > currentCellCount) && loadPageRequestFlag{
                    whichApiDataToprocess = ""
                    pageNumber += 1
                    loadPageRequestFlag = false
                    callingHttppApi()
                }
            }
        }
    }
    
    @objc func viewDetails(_ recognizer: UITapGestureRecognizer) {
        reviewId = myProductReviewViewModel.getMyProductReviewData[(recognizer.view?.tag)!].id
        self.performSegue(withIdentifier: "reviewDetails", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier! == "reviewDetails") {
            let viewController:ReviewDetails = segue.destination as UIViewController as! ReviewDetails
            viewController.reviewId = reviewId
        }
    }
}
