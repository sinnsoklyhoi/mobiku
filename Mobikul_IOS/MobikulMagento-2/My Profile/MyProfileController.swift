//
//  MyProfileController.swift
//  Magento2V4Theme
//
//  Created by Webkul on 10/02/18.
//  Copyright © 2018 Webkul. All rights reserved.
//

import UIKit
import Alamofire

class MyProfileController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var myProfileTableView: UITableView!
    
    var logoutData:NSMutableArray = ["signin".localized, "notification".localized, "language".localized,"Currency".localized, "searchterms".localized,  "advancesearchterms".localized,"comparelist".localized,"contactus".localized,"cmspages".localized]
    var signInData:NSMutableArray = ["myorder".localized, "Currency".localized ,"language".localized, "searchterms".localized, "advancesearchterms".localized, "mywishlist".localized, "myproductreviews".localized, "accountinformation".localized, "addressbook".localized, "comparelist".localized, "mydownload".localized, "contactus".localized, "cmspages".localized]
    var extraData = NSMutableArray()
    var cmsData:NSMutableArray = []
    var sinInView:NSMutableArray = [""]
    var profileData:NSMutableArray = []
    var showUserProfile:Bool = false
    var homeViewModel : HomeViewModel!
    var imageForCategoryMenu:String = ""
    var imageData:NSData!
    var cmsID:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myProfileTableView.register(UINib(nibName: "ProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileTableViewCell")
        myProfileTableView.register(UINib(nibName: "ProfileCell", bundle: nil), forCellReuseIdentifier: "ProfileCell")
        myProfileTableView.rowHeight = UITableViewAutomaticDimension
        self.myProfileTableView.estimatedRowHeight = 50
        self.myProfileTableView.separatorColor = UIColor.clear
        let paymentViewNavigationController = self.tabBarController?.viewControllers?[0]
        let nav = paymentViewNavigationController as! UINavigationController;
        let paymentMethodViewController = nav.viewControllers[0] as! ViewController
        homeViewModel = paymentMethodViewController.homeViewModel;
        
        cmsData = []
        if homeViewModel.cmsData.count > 0{
            for i in 0..<homeViewModel.cmsData.count{
                cmsData.add(homeViewModel.cmsData[i].title)
            }
        }
        
        if homeViewModel.gdprData.gdprEnable && homeViewModel.gdprData.tncHomepageEnable    {
            //Notification Realted is enabled
            extraData = ["logout".localized, "notificationrelatedinformation".localized]
        }else{
            //Notification Realted is disabled
            extraData = ["logout".localized]
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if defaults.object(forKey: "customerId") != nil{
            self.navigationController?.isNavigationBarHidden = true
        }else{
            self.navigationController?.isNavigationBarHidden = false
        }
        if defaults.object(forKey: "customerId") != nil{
            showUserProfile = true
            profileData = [sinInView,logoutData, signInData,extraData]
            myProfileTableView.dataSource = self
            myProfileTableView.delegate = self
            DispatchQueue.main.async {
                self.myProfileTableView.reloadData()
            }
        }else{
            showUserProfile = false
            profileData = [sinInView,logoutData]
            myProfileTableView.dataSource = self 
            myProfileTableView.delegate = self
//            DispatchQueue.main.async {
                self.myProfileTableView.reloadData()
//            }
            self.myProfileTableView.setContentOffset(CGPoint(x: 0, y: -120), animated: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
    }
    
    func logoutAPI(){
        var requstParams = [String:Any]();
        GlobalData.sharedInstance.showLoader()
        if defaults.object(forKey: "storeId") != nil{
            requstParams["storeId"] = defaults.object(forKey: "storeId") as! String
        }
        let deviceToken = defaults.object(forKey:"deviceToken");
        if deviceToken != nil{
            requstParams["token"] = deviceToken
        }
        
        let customerId = defaults.object(forKey:"customerId");
        if customerId != nil{
            requstParams["customerToken"] = customerId
        }
        
        GlobalData.sharedInstance.callingHttpRequest(params:requstParams, apiname:"mobikulhttp/extra/logout", currentView: self){success,responseObject in
            if success == 1{
                
                GlobalData.sharedInstance.dismissLoader()
                self.view.isUserInteractionEnabled = true
                
                for key in UserDefaults.standard.dictionaryRepresentation().keys {
                    if(key.description == "storeId"||key.description == "language"||key.description == "AppleLanguages" || key.description == "currency" || key.description == "authKey" || key.description == "TouchEmailId" || key.description == "TouchPasswordValue" || key.description == "touchIdFlag" || key.description == "deviceToken"){
                        continue
                    }else{
                        UserDefaults.standard.removeObject(forKey: key.description)
                    }
                }
                UserDefaults.standard.synchronize()
                self.tabBarController!.tabBar.items?[3].badgeValue = nil
                self.viewWillAppear(true)
                
            }else if success == 2{
                self.logoutAPI()
                GlobalData.sharedInstance.dismissLoader()
            }
        }
    }
    
    @objc func uploadProfileORBannerPic(){
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let uploadBanner = UIAlertAction(title: GlobalData.sharedInstance.language(key: "uploadbanner"), style: .default, handler: uploadBannerPic)
        let uploadPic = UIAlertAction(title: GlobalData.sharedInstance.language(key: "uploadpic"), style: .default, handler: uploadProfilePic)
        let cancel = UIAlertAction(title: GlobalData.sharedInstance.language(key: "cancel"), style: .cancel, handler: cancelDeletePlanet)
        alert.addAction(uploadBanner)
        alert.addAction(uploadPic)
        alert.addAction(cancel)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x:self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width : 1.0, height : 1.0)
        self.present(alert, animated: true, completion: nil)
    }
    
    func uploadBannerPic(alertAction: UIAlertAction!) {
        imageForCategoryMenu = "uploadBannerPic"
        let AC = UIAlertController(title: GlobalData.sharedInstance.language(key: "uploadbanner"), message: "", preferredStyle: .alert)
        let clickBtn = UIAlertAction(title: GlobalData.sharedInstance.language(key: "takepicture") , style: .default, handler: {(_ action: UIAlertAction) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        })
        
        let cameraRollBtn = UIAlertAction(title: GlobalData.sharedInstance.language(key: "uploadfromcameraroll"), style: .default, handler: {(_ action: UIAlertAction) -> Void in
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.allowsEditing = true
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true, completion: {  })
        })
        let noBtn = UIAlertAction(title: GlobalData.sharedInstance.language(key: "cancel"), style: .default, handler: {(_ action: UIAlertAction) -> Void in
        })
        AC.addAction(clickBtn)
        AC.addAction(cameraRollBtn)
        AC.addAction(noBtn)
        self.parent!.present(AC, animated: true, completion: {  })
    }
    
    func uploadProfilePic(alertAction: UIAlertAction!) {
        imageForCategoryMenu = "profilePicture";
        let AC = UIAlertController(title: GlobalData.sharedInstance.language(key: "uploadpic"), message: "", preferredStyle: .alert)
        let clickBtn = UIAlertAction(title: GlobalData.sharedInstance.language(key: "takepicture"), style: .default, handler: {(_ action: UIAlertAction) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        })
        let cameraRollBtn = UIAlertAction(title: GlobalData.sharedInstance.language(key: "uploadfromcameraroll"), style: .default, handler: {(_ action: UIAlertAction) -> Void in
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.allowsEditing = true
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true, completion: {  })
        })
        let noBtn = UIAlertAction(title: GlobalData.sharedInstance.language(key: "cancel"), style: .default, handler: {(_ action: UIAlertAction) -> Void in
        })
        AC.addAction(clickBtn)
        AC.addAction(cameraRollBtn)
        AC.addAction(noBtn)
        self.parent!.present(AC, animated: true, completion: {  })
    }
    
    func cancelDeletePlanet(alertAction: UIAlertAction!) {
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let imageNew = image.fixOrientation()
            imageData = UIImageJPEGRepresentation(imageNew, 0.5) as NSData?
            saveProfileData()
        }
        picker.dismiss(animated: true, completion: nil);
    }
    
    func saveProfileData() {
        GlobalData.sharedInstance.showLoader()
        
        var headers: HTTPHeaders = [:]
        if defaults.object(forKey: "authKey") == nil{
            headers = [
                "apiKey": API_USER_NAME,
                "apiPassword": API_KEY,
                "authKey":""
            ]
        }else{
            headers = [
                "apiKey": API_USER_NAME,
                "apiPassword": API_KEY,
                "authKey":defaults.object(forKey: "authKey") as! String
            ]
        }
        
        var imageSendUrl:String = ""
        if (imageForCategoryMenu == "uploadBannerPic") {
            let url = "\(BASE_DOMAIN)/mobikulhttp/index/uploadBannerPic"
            imageSendUrl = url
        }else {
            let url = "\(BASE_DOMAIN)/mobikulhttp/index/uploadProfilePic"
            imageSendUrl = url
        }
        
        DispatchQueue.main.async {
            Alamofire.upload(multipartFormData: { multipartFormData in
                let customerId = defaults.object(forKey:"customerId")
                var params = [String:AnyObject]()
                
                params["customerToken"] = customerId as AnyObject
                let width = String(format:"%f", SCREEN_WIDTH * UIScreen.main.scale)
                params["width"] = width as AnyObject
                
                print(params)
                for (key, value) in params {
                    if let data = value.data(using: String.Encoding.utf8.rawValue) {
                        multipartFormData.append(data, withName: key)
                    }
                }
                
                if self.imageData != nil{
                    let imageData1 = self.imageData! as Data
                    multipartFormData.append(imageData1, withName: "imageFormKey", fileName: "image.jpg", mimeType: "image/jpeg")
                }
            },
                             to: imageSendUrl,method:HTTPMethod.post,
                             headers: headers, encodingCompletion: { encodingResult in
                                switch encodingResult {
                                case .success(let upload, _, _):
                                    upload
                                        .validate()
                                        .responseJSON { response in
                                            switch response.result {
                                            case .success(let value):
                                                print("responseObject: \(value)")
                                                var dict = JSON(value)
                                                //                                                GlobalData.sharedInstance.showSuccessSnackBar(msg:dict["message"].stringValue )
                                                GlobalData.sharedInstance.dismissLoader()
                                                
                                                if(self.imageForCategoryMenu == "uploadBannerPic"){
                                                    let imageUrl = dict["url"].stringValue
                                                    defaults.set(imageUrl, forKey: "profileBanner")
                                                    self.myProfileTableView.reloadData()
                                                }else{
                                                    let imageUrl = dict["url"].stringValue
                                                    defaults.set(imageUrl, forKey: "profilePicture")
                                                    self.myProfileTableView.reloadData()
                                                }
                                                
                                            case .failure(let responseError):
                                                GlobalData.sharedInstance.showErrorSnackBar(msg: "Not updated")
                                                GlobalData.sharedInstance.dismissLoader()
                                                print("responseError: \(responseError)")
                                            }
                                    }
                                case .failure(let encodingError):
                                    print("encodingError: \(encodingError)")
                                }
            })
            
        }
    }
    
    func showCMSData(){
        let alert = UIAlertController(title: GlobalData.sharedInstance.language(key: "seeprivacypolicy"), message: nil, preferredStyle: .actionSheet)
        for i in 0..<homeViewModel.cmsData.count{
            let str : String = homeViewModel.cmsData[i].title
            let action = UIAlertAction(title: str, style: .default, handler: {(_ action: UIAlertAction) -> Void in
                self.selectCmsData(pos:i)
            })
            alert.addAction(action)
        }
        let cancel = UIAlertAction(title: GlobalData.sharedInstance.language(key: "cancel"), style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
        })
        alert.addAction(cancel)
        
        // Support display in iPad
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x:self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width : 1.0, height : 1.0)
        self.present(alert, animated: true, completion: nil)
    }
    
    func selectCmsData(pos:Int){
        cmsID = homeViewModel.cmsData[pos].id
        self.performSegue(withIdentifier: "cmsdata", sender: self)
    }
    
    func openCurrencyView(){
        let alert = UIAlertController(title: GlobalData.sharedInstance.language(key: "chooseyourcurrency"), message: nil, preferredStyle: .actionSheet)
        for i in 0..<homeViewModel.currency.count{
            var image = UIImage(named: "")
            if defaults.object(forKey: "currency") != nil{
                let currencyCode = defaults.object(forKey: "currency") as! String
                if currencyCode == homeViewModel.currency[i] as! String{
                    image = UIImage(named: "ic_check")
                }else{
                    image = UIImage(named: "")
                }
            }
            
            let str : String = homeViewModel.currency[i] as! String
            let action = UIAlertAction(title: str, style: .default, handler: {(_ action: UIAlertAction) -> Void in
                self.selectCurrencyData(pos:i)
            })
            action.setValue(image, forKey: "image")
            alert.addAction(action)
        }
        
        let cancel = UIAlertAction(title: GlobalData.sharedInstance.language(key: "cancel"), style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
        })
        alert.addAction(cancel)
        
        // Support display in iPad
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x:self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width : 1.0, height : 1.0)
        self.present(alert, animated: true, completion: nil)
    }
    
    func selectCurrencyData(pos:Int){
        let productModel = ProductViewModel()
        
        if let currentCurrency =  defaults.object(forKey: "currency") as? String{
            if currentCurrency != homeViewModel.currency[pos] as! String{
                productModel.deleteAllRecentViewProductData()
            }
        }
        
        let contentForThisRow  = homeViewModel.currency[pos] as! String
        defaults.set(contentForThisRow, forKey: "currency")
        defaults.synchronize()
        
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        rootviewcontroller.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "rootnav")
        let mainwindow = (UIApplication.shared.delegate?.window!)!
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainwindow, duration: 0.55001, options: .transitionFlipFromLeft, animations: { () -> Void in

        }) { (finished) -> Void in
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier! == "storeview") {
            let viewController:StoreViewController = segue.destination as UIViewController as! StoreViewController
            viewController.storeData = homeViewModel.storeData
            
        }else if (segue.identifier! == "cmsdata") {
            let viewController:CMSPageData = segue.destination as UIViewController as! CMSPageData
            viewController.cmsId = cmsID
        }
    }
    
    /// Takes the screenshot of the screen and returns the corresponding image
    ///
    /// - Parameter shouldSave: Boolean flag asking if the image needs to be saved to user's photo library. Default set to 'true'
    /// - Returns: (Optional)image captured as a screenshot
    open func takeScreenshot() -> UIImage? {
        self.tabBarController?.tabBar.isHidden = true
        var screenshotImage :UIImage?
        let layer = UIApplication.shared.keyWindow!.layer
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
        guard let context = UIGraphicsGetCurrentContext() else {return nil}
        layer.render(in:context)
        screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.tabBarController?.tabBar.isHidden = false
        return screenshotImage
    }
}

extension UIImage {
    func fixOrientation() -> UIImage {
        if self.imageOrientation == UIImageOrientation.up {
            return self
        }
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        self.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        if let normalizedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            return normalizedImage
        }else {
            return self
        }
    }
}

//MARK:- UITableViewDelegate, UITableViewDataSource
extension MyProfileController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }else{
            return 20
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && showUserProfile{
            return 250
        }else if indexPath.section == 0 && !showUserProfile{
            return 0
        }else{
            return 60
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return profileData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 && showUserProfile{
            return 1
        }else if section == 0 && !showUserProfile{
            return 0
        }else if section == 1 && showUserProfile{
            return 0
        }else{
            return (profileData[section] as AnyObject).count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell:ProfileTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell") as! ProfileTableViewCell
            if defaults.object(forKey: "customerEmail") != nil{
                cell.profileEmail.text = defaults.object(forKey: "customerEmail") as? String
            }
            if defaults.object(forKey: "customerName") != nil{
                cell.profileName.text = defaults.object(forKey: "customerName") as? String
            }
            if defaults.object(forKey: "profilePicture") != nil{
                let imageUrl = defaults.object(forKey: "profilePicture") as? String
                cell.profileImage.image = #imageLiteral(resourceName: "ic_placeholder.png")
                GlobalData.sharedInstance.getImageFromUrl(imageUrl: imageUrl!, imageView: cell.profileImage)
            }
            if defaults.object(forKey: "profileBanner") != nil{
                let imageUrl = defaults.object(forKey: "profileBanner") as? String
                GlobalData.sharedInstance.getImageFromUrl(imageUrl: imageUrl!, imageView: cell.profileBannerImage)
            }
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MyProfileController.uploadProfileORBannerPic))
            cell.editView.addGestureRecognizer(tap)
            
            cell.selectionStyle = .none
            return cell
        }else{
            let cell:ProfileCell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell") as! ProfileCell
            let sectionContents:NSMutableArray = profileData[indexPath.section] as! NSMutableArray
            let contentForThisRow  = sectionContents[indexPath.row]
            cell.name.text = contentForThisRow as? String
            
            if indexPath.section == 1{
                if indexPath.row == 0{
                    cell.profileImage.image = UIImage(named: "ic_signin")!
                }else if indexPath.row == 1{
                    cell.profileImage.image = UIImage(named: "ic_notificationprofile")!
                }else if indexPath.row == 2{
                    cell.profileImage.image = UIImage(named: "ic_language")!
                }else if indexPath.row == 3{
                    cell.profileImage.image = UIImage(named: "ic_currency")!
                }else if indexPath.row == 4{
                    cell.profileImage.image = UIImage(named: "ic_searchterms")!
                }else if indexPath.row == 5{
                    cell.profileImage.image = UIImage(named: "ic_advancesearch")!
                }else if indexPath.row == 6{
                    cell.profileImage.image = UIImage(named: "ic_profilecompare")!
                }else if indexPath.row == 7{
                    cell.profileImage.image = UIImage(named: "ic_contactus")!
                }else if indexPath.row == 8{
                    cell.profileImage.image = UIImage(named: "ic_accountinfo")!
                }
            }else if indexPath.section == 2{
                if indexPath.row == 0{
                    cell.profileImage.image = UIImage(named: "ic_myorder")!
                }else if indexPath.row == 1{
                    cell.profileImage.image = UIImage(named: "ic_currency")!
                }else if indexPath.row == 2{
                    cell.profileImage.image = UIImage(named: "ic_language")!
                }else if indexPath.row == 3{
                    cell.profileImage.image = UIImage(named: "ic_searchterms")!
                }else if indexPath.row == 4{
                    cell.profileImage.image = UIImage(named: "ic_advancesearch")!
                }else if indexPath.row == 5{
                    cell.profileImage.image = UIImage(named: "ic_mywishlist")!
                }else if indexPath.row == 6{
                    cell.profileImage.image = UIImage(named: "ic_productreviews")!
                }else if indexPath.row == 7{
                    cell.profileImage.image = UIImage(named: "ic_accountinfo")!
                }else if indexPath.row == 8{
                    cell.profileImage.image = UIImage(named: "ic_addressbook")!
                }else if indexPath.row == 9{
                    cell.profileImage.image = UIImage(named: "ic_profilecompare")!
                }else if indexPath.row == 10{
                    cell.profileImage.image = UIImage(named: "ic_downloadable")!
                }else if indexPath.row == 11{
                    cell.profileImage.image = UIImage(named: "ic_contactus")!
                }else if indexPath.row == 12{
                    cell.profileImage.image = UIImage(named: "ic_accountinfo")!
                }
            }else if indexPath.section == 3{
                if indexPath.row == 0{
                    cell.profileImage.image = UIImage(named: "ic_logout")!
                }else{
                    //GDPR : Notification Related Info
                    cell.profileImage.image = nil
                }
            }
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            if indexPath.row == 0{
                self.performSegue(withIdentifier: "customerlogin", sender: self)
            }else if indexPath.row == 1{
                self.performSegue(withIdentifier: "notification", sender: self)
            }else if indexPath.row == 2{
                self.performSegue(withIdentifier: "storeview", sender: self)
            }else if indexPath.row == 3{
                self.openCurrencyView()
            }else if indexPath.row == 4{
                //search terms
                self.performSegue(withIdentifier: "searchterms", sender: self)
            }else if indexPath.row == 5{
                //advance search terms
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AdvanceSearch") as! AdvanceSearch
                self.navigationController?.pushViewController(vc, animated: true)
            }else if indexPath.row == 6{
                self.performSegue(withIdentifier: "comparelist", sender: self)
            }else if indexPath.row == 7{
                if let contactUsVc = self.storyboard?.instantiateViewController(withIdentifier: "ContactUSController") as? ContactUSController{
                    self.navigationController?.pushViewController(contactUsVc, animated: true)
                }
            }else if indexPath.row == 8{
                self.showCMSData()
            }
        }else if indexPath.section == 2{
            if indexPath.row == 0{
                self.performSegue(withIdentifier: "myorder", sender: self)
            }else if indexPath.row == 1{
                self.openCurrencyView()
            }else if indexPath.row == 2{
                self.performSegue(withIdentifier: "storeview", sender: self)
            }else if indexPath.row == 3{
                //search terms
                self.performSegue(withIdentifier: "searchterms", sender: self)
            }else if indexPath.row == 4{
                //advance search terms
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AdvanceSearch") as! AdvanceSearch
                self.navigationController?.pushViewController(vc, animated: true)
            }else if indexPath.row == 5{
                self.performSegue(withIdentifier: "mywishlist", sender: self)
            }else if indexPath.row == 6{
                self.performSegue(withIdentifier: "myproductreviews", sender: self)
            }else if indexPath.row == 7{
                //account information
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AccountInformation") as! AccountInformation
                self.navigationController?.pushViewController(vc, animated: true)
            }else if indexPath.row == 8{
                self.performSegue(withIdentifier: "addressbook", sender: self)
            }else if indexPath.row == 9{
                self.performSegue(withIdentifier: "comparelist", sender: self)
            }else if indexPath.row == 10{
                //downloadable products
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyDownloads") as! MyDownloads
                self.navigationController?.pushViewController(vc, animated: true)
            }else if indexPath.row == 11{
                if let contactUsVc = self.storyboard?.instantiateViewController(withIdentifier: "ContactUSController") as? ContactUSController{
                    self.navigationController?.pushViewController(contactUsVc, animated: true)
                }
            }else if indexPath.row == 12{
                self.showCMSData()
            }
        }else if indexPath.section == 3 {
            
            if indexPath.row == 0   {
                //Logout
                let AC = UIAlertController(title: GlobalData.sharedInstance.language(key: "warninglogoutmessage"), message: "", preferredStyle: .alert)
                let ok = UIAlertAction(title: GlobalData.sharedInstance.language(key: "yes"), style: .default, handler: {(_ action: UIAlertAction) -> Void in
                    self.logoutAPI()
                })
                
                let noBtn = UIAlertAction(title: GlobalData.sharedInstance.language(key: "no"), style: .destructive, handler: {(_ action: UIAlertAction) -> Void in
                })
                AC.addAction(ok)
                AC.addAction(noBtn)
                self.parent!.present(AC, animated: true, completion: {  })
            }else{
                //GDPR : Notification Related Info
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "GDPRWebViewController") as! GDPRWebViewController
                vc.backImg = takeScreenshot()
                vc.content = homeViewModel.gdprData.tncHomepageContent
                self.navigationController?.present(vc, animated: true, completion: nil)
            }
        }
    }
}
