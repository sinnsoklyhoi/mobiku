//
//  AddressBookController.swift
//  Magento2V4Theme
//
//  Created by Webkul on 12/02/18.
//  Copyright © 2018 Webkul. All rights reserved.
//

import UIKit

class AddressBookController: UIViewController,UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet weak var addnewAddressButton: UIButton!
    @IBOutlet weak var addressTableView: UITableView!
   
    
    var addressBookViewModel:AddressBookViewModel!
    var addoredit:String = ""
    var addressId:String = "0"
    var whichApiDataToProcess:String = ""
    var documentPathUrl: NSURL!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.title = "addressbook".localized
        addressTableView.register(UINib(nibName: "AddressViewCell", bundle: nil), forCellReuseIdentifier: "AddressViewCell")
        addressTableView.register(UINib(nibName: "AddreessViewCell2", bundle: nil), forCellReuseIdentifier: "AddreessViewCell2")
        addressTableView.register(UINib(nibName: "AddressViewCell3", bundle: nil), forCellReuseIdentifier: "AddressViewCell3")
        addressTableView.rowHeight = UITableViewAutomaticDimension
        self.addressTableView.estimatedRowHeight = 100
        self.addressTableView.separatorColor = UIColor.clear
        addnewAddressButton.setTitleColor(UIColor().HexToColor(hexString: BUTTON_COLOR), for: .normal)
        addnewAddressButton.setTitle("addnewaddress".localized, for: .normal)
        addnewAddressButton.layer.borderColor = UIColor().HexToColor(hexString: BUTTON_COLOR).cgColor
        addnewAddressButton.layer.borderWidth = 0.3
        addnewAddressButton.layer.cornerRadius = 15
        addnewAddressButton.layer.masksToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        callingHttppApi()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
    }
    
    func callingHttppApi(){
        GlobalData.sharedInstance.showLoader()
        self.view.isUserInteractionEnabled = false
        var requstParams = [String:Any]();
        if defaults.object(forKey: "storeId") != nil{
            requstParams["storeId"] = defaults.object(forKey: "storeId") as! String
        }
        requstParams["customerToken"] = defaults.object(forKey:"customerId") as! String
        
        if whichApiDataToProcess == "deleteAddress"{
            var requstParams = [String:Any]()
            
            let customerId = defaults.object(forKey: "customerId")
            if(customerId != nil){
                requstParams["customerToken"] = customerId as! String
            }
            
            requstParams["addressId"] = addressId
            
            GlobalData.sharedInstance.callingHttpRequest(params:requstParams, apiname:"mobikulhttp/customer/deleteAddress", currentView: self){success,responseObject in
                if success == 1{
                    
                    let data = responseObject as! NSDictionary
                    print(data)
                    GlobalData.sharedInstance.dismissLoader()
                    let errorCode = data .object(forKey:"success") as! Bool
                    if errorCode == true{
                        self.whichApiDataToProcess = ""
                        self.callingHttppApi()
                    }else{
                        GlobalData.sharedInstance.showErrorSnackBar(msg: GlobalData.sharedInstance.language(key: "somethingwentwrongpleasetryagain"))
                    }
                }else if success == 2{
                    GlobalData.sharedInstance.dismissLoader()
                    self.callingHttppApi()
                }
            }
        }else{
            GlobalData.sharedInstance.callingHttpRequest(params:requstParams, apiname:"mobikulhttp/customer/addressBookData", currentView: self){success,responseObject in
                if success == 1{
                    if responseObject?.object(forKey: "storeId") != nil{
                        let storeId:String = String(format: "%@", responseObject!.object(forKey: "storeId") as! CVarArg)
                        if storeId != "0"{
                            defaults .set(storeId, forKey: "storeId")
                        }
                    }
                    
                    GlobalData.sharedInstance.dismissLoader()
                    self.view.isUserInteractionEnabled = true
                    print(responseObject!)
                    self.addressBookViewModel = AddressBookViewModel(data: JSON(responseObject as! NSDictionary))
                    self.doFurtherProcessingWithResult()
                    
                }else if success == 2{
                    GlobalData.sharedInstance.dismissLoader()
                    self.callingHttppApi()
                }
            }
        }
    }
    
    func doFurtherProcessingWithResult(){
        addressTableView.delegate = self
        addressTableView.dataSource = self
        addressTableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if section == 0 || section == 1{
            return 1
        }else{
            return self.addressBookViewModel.additionalAddressCollection.count;
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            return GlobalData.sharedInstance.language(key: "BillingAddress")
        }else if section == 1{
            return GlobalData.sharedInstance.language(key: "ShippingAddress")
        }else{
            return GlobalData.sharedInstance.language(key: "AdditionalAddress")
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0   {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddressViewCell", for: indexPath) as! AddressViewCell
            cell.addressValue.text = self.addressBookViewModel.addressBookModel.billingAddress
            cell.deleteButton.isHidden = true
            cell.editButton.tag = indexPath.row
            cell.editButton.addTarget(self, action: #selector(editBillingAddressButtonTapped(sender:)), for: .touchUpInside)
            cell.downloadBtn.superview?.tag = indexPath.section
            cell.downloadBtn.tag = indexPath.row
            cell.downloadBtn.addTarget(self, action: #selector(downloadFile(_:)), for: .touchUpInside)
            cell.item = self.addressBookViewModel
            
            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddreessViewCell2", for: indexPath) as! AddreessViewCell2
            cell.addressValue.text = self.addressBookViewModel.addressBookModel.shippingAddress
            cell.deleteButton.isHidden = true
            cell.editButton.tag = indexPath.row
            cell.editButton.addTarget(self, action: #selector(editShippingAddressButtonTapped(sender:)), for: .touchUpInside)
            cell.downloadButton.superview?.tag = indexPath.section
            cell.downloadButton.tag = indexPath.row
            cell.downloadButton.addTarget(self, action: #selector(downloadFile(_:)), for: .touchUpInside)
            cell.item = self.addressBookViewModel
            
            cell.selectionStyle = .none
            return cell
        }else   {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddressViewCell3", for: indexPath) as! AddressViewCell3
            cell.addressValue.text = self.addressBookViewModel.additionalAddressCollection[indexPath.row].value
            cell.editButton.tag = indexPath.row
            cell.editButton.addTarget(self, action: #selector(editAddressButtonTapped(sender:)), for: .touchUpInside)
            cell.deleteButton.isHidden = false
            cell.deleteButton.tag = indexPath.row
            cell.deleteButton.addTarget(self, action: #selector(deleteAddress(sender:)), for: .touchUpInside)
            cell.downloadButton.superview?.tag = indexPath.section
            cell.downloadButton.tag = indexPath.row
            cell.downloadButton.addTarget(self, action: #selector(downloadFile(_:)), for: .touchUpInside)
            cell.item = self.addressBookViewModel
            
            cell.selectionStyle = .none
            return cell
        }
    }
    
    @IBAction func AddnewAddressClick(_ sender: UIButton) {
        addoredit = "0"
        self.performSegue(withIdentifier: "addeditaddress", sender: self)
    }
    
    @objc func editAddressButtonTapped(sender: UIButton){
        addoredit = "1";
        addressId = self.addressBookViewModel.additionalAddressCollection[sender.tag].id
        self.performSegue(withIdentifier: "addeditaddress", sender: self)
    }
    
    @objc func editShippingAddressButtonTapped(sender: UIButton){
        if self.addressBookViewModel.addressBookModel.shippingId == 0{
            addoredit = "0"
        }else{
            addoredit = "1"
            addressId = self.addressBookViewModel.addressBookModel.shippingIdValue
        }
        self.performSegue(withIdentifier: "addeditaddress", sender: self)
    }
    
    @objc func editBillingAddressButtonTapped(sender: UIButton) {
        if self.addressBookViewModel.addressBookModel.billingId == 0    {
            addoredit = "0"
        }else{
            addoredit = "1"
            addressId = self.addressBookViewModel.addressBookModel.billingIdValue
        }
        self.performSegue(withIdentifier: "addeditaddress", sender: self)
    }
    
    @objc func deleteAddress(sender: UIButton){
        whichApiDataToProcess = "deleteAddress";
        addressId = self.addressBookViewModel.additionalAddressCollection[sender.tag].id
        
        let AC = UIAlertController(title:GlobalData.sharedInstance.language(key: "warning"), message: GlobalData.sharedInstance.language(key: "cartemtyinfo"), preferredStyle: .alert)
        let okBtn = UIAlertAction(title: GlobalData.sharedInstance.language(key: "ok"), style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.callingHttppApi()
        })
        let noBtn = UIAlertAction(title:GlobalData.sharedInstance.language(key: "cancel"), style: .destructive, handler: {(_ action: UIAlertAction) -> Void in
        })
        AC.addAction(okBtn)
        AC.addAction(noBtn)
        self.parent!.present(AC, animated: true, completion: { })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier! == "addeditaddress") {
            let viewController:AddEditAddress = segue.destination as UIViewController as! AddEditAddress
            viewController.addOrEdit = self.addoredit
            viewController.addressId = addressId
        }
    }
}

//MARK:- GDPR
extension AddressBookController {
    @IBAction func downloadFile(_ sender: UIButton)    {
        
        if sender.superview?.tag == 0{
            addressId = self.addressBookViewModel.addressBookModel.billingIdValue
        }else if sender.superview?.tag == 1{
            addressId = self.addressBookViewModel.addressBookModel.shippingIdValue
        }else if sender.superview?.tag == 2{
            addressId = self.addressBookViewModel.additionalAddressCollection[sender.tag].id
        }
        
        var fileName: String = ""
        let date = Date()
        let formatter = DateFormatter()
        
        formatter.dateFormat = "dd.MM.yyyy_HH:mm:ss"
        let  url: String = HOST_NAME+"mobikulgdpr/pdf/getaddressinfo"
        fileName = "gdprAddressInfo"+formatter.string(from: date)+".pdf"
        
        let post = NSMutableString()
        post .appendFormat("addressId=%@&", addressId as CVarArg)
        if let customerId = defaults.object(forKey: "customerId") as? String{
            post .appendFormat("customerToken=%@&", customerId as CVarArg)
        }
        if let storeId = defaults.object(forKey: "storeId") as? String  {
            post .appendFormat("storeId=%@&", storeId as CVarArg)
        }
        
        self.load(url: URL(string: url)!, params: post as String, name: fileName)
    }
    
    func load(url: URL, params: String, name: String) {
        GlobalData.sharedInstance.showLoader()
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let postString = params;
        request.httpBody = postString.data(using: .utf8)
        
        if defaults.object(forKey: "authKey") == nil{
            request.addValue("", forHTTPHeaderField: "authKey")
        }else{
            request.addValue(defaults.object(forKey: "authKey") as! String, forHTTPHeaderField: "authKey")
        }
        
        request.addValue(API_USER_NAME, forHTTPHeaderField: "apiKey")
        request.addValue(API_KEY, forHTTPHeaderField: "apiPassword")
        
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                // Success
                
                guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode != 404 else {
                    GlobalData.sharedInstance.showErrorSnackBar(msg: "filenotfounderror404".localized)
                    GlobalData.sharedInstance.dismissLoader()
                    return
                }
                
                GlobalData.sharedInstance.dismissLoader()
                print("Success: \(statusCode)")
                
                do {
                    let largeImageData = try Data(contentsOf: tempLocalUrl)
                    let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
                    let fileURL = documentsDirectoryURL.appendingPathComponent(name)
                    
                    if !FileManager.default.fileExists(atPath: fileURL.path) {
                        do {
                            try largeImageData.write(to: fileURL)
                            let AC = UIAlertController(title: "success".localized, message: "filesavemessage".localized, preferredStyle: .alert)
                            let okBtn = UIAlertAction(title: "ok".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
                                self.documentPathUrl = fileURL as NSURL
                                
                                if let gdprPdfVC = self.storyboard?.instantiateViewController(withIdentifier: "ShowDownloadFile") as? ShowDownloadFile{
                                    gdprPdfVC.documentUrl = self.documentPathUrl
                                    self.navigationController?.pushViewController(gdprPdfVC, animated: true)
                                }
                            })
                            let noBtn = UIAlertAction(title: "cancel".localized, style: .destructive, handler: {(_ action: UIAlertAction) -> Void in
                            })
                            AC.addAction(okBtn)
                            AC.addAction(noBtn)
                            self.present(AC, animated: true, completion: { })
                        } catch {
                            print(error)
                        }
                    } else {
                        print("Image Not Added")
                    }
                } catch {
                    print("error")
                }
                do {
                } catch (let writeError) {
                }
            } else {
                GlobalData.sharedInstance.dismissLoader()
                print("Failure: %@", error?.localizedDescription)
            }
        }
        task.resume()
    }
}
