//
/**
MobikulMagento-2
@Category Webkul
@author Webkul <support@webkul.com>
FileName: Productcategory+UICollectionView.swift
Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
@license https://store.webkul.com/license.html ASL Licence
@link https://store.webkul.com/license.html

*/


import Foundation

extension Productcategory : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout    {
    
    //MARK:- UICollectionView
    
    func collectionView(_ view: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        //        if (productCollectionViewModel.getProductCollectionData.count > 0)   {
        //            sortFilterSuperView.isHidden = false
        //        }else{
        //            sortFilterSuperView.isHidden = true
        //        }
        
        return productCollectionViewModel.getProductCollectionData.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if change == true{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productimagecell", for: indexPath) as! ProductImageCell
            cell.layer.borderColor = UIColor().HexToColor(hexString: LIGHTGREY).cgColor
            cell.layer.borderWidth = 0.5
            cell.productImage.image = UIImage(named: "ic_placeholder.png")
            cell.productName.text = productCollectionViewModel.getProductCollectionData[indexPath.row].productName
            cell.productPrice.text =  productCollectionViewModel.getProductCollectionData[indexPath.row].price
            GlobalData.sharedInstance.getImageFromUrl(imageUrl:productCollectionViewModel.getProductCollectionData[indexPath.row].productImage , imageView: cell.productImage)
            cell.specialPrice.isHidden = true
            if productCollectionViewModel.getProductCollectionData[indexPath.row].isInWishlist == true{
                cell.wishListButton.setImage(UIImage(named: "ic_wishlist_fill")!, for: .normal)
            }else{
                cell.wishListButton.setImage(UIImage(named: "ic_wishlist_empty")!, for: .normal)
            }
            
            cell.wishListButton.tag = indexPath.row
            cell.wishListButton.addTarget(self, action: #selector(addToWishList(sender:)), for: .touchUpInside)
            cell.wishListButton.isUserInteractionEnabled = true
            
            cell.addToCompareButton.tag = indexPath.row
            cell.addToCompareButton.addTarget(self, action: #selector(addToCompare(sender:)), for: .touchUpInside)
            cell.addToCompareButton.isUserInteractionEnabled = true
            
            if productCollectionViewModel.getProductCollectionData[indexPath.row].typeID == "grouped"{
                cell.productPrice.text =  productCollectionViewModel.getProductCollectionData[indexPath.row].groupedPrice
                
            }else if productCollectionViewModel.getProductCollectionData[indexPath.row].typeID == "bundle"{
                cell.productPrice.text =  productCollectionViewModel.getProductCollectionData[indexPath.row].formatedMinPrice+" - "+productCollectionViewModel.getProductCollectionData[indexPath.row].formatedMaxPrice
            }else{
                if productCollectionViewModel.getProductCollectionData[indexPath.row].isInRange == true{
                    if productCollectionViewModel.getProductCollectionData[indexPath.row].specialPrice < productCollectionViewModel.getProductCollectionData[indexPath.row].normalprice{
                        cell.productPrice.text = productCollectionViewModel.getProductCollectionData[indexPath.row].formatedSpecialPrice
                        let attributeString = NSMutableAttributedString(string: productCollectionViewModel.getProductCollectionData[indexPath.row].formatedPrice)
                        attributeString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 1, range: NSRange(location: 0, length: attributeString.length))
                        cell.specialPrice.attributedText = attributeString
                        cell.specialPrice.isHidden = false
                    }else{
                        cell.productPrice.text =  productCollectionViewModel.getProductCollectionData[indexPath.row].price
                        cell.specialPrice.isHidden = true
                    }
                }
            }
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "listcollectionview", for: indexPath) as! ListCollectionViewCell
            cell.layer.borderColor = UIColor().HexToColor(hexString: LIGHTGREY).cgColor
            cell.layer.borderWidth = 0.5
            cell.imageView.image = UIImage(named: "ic_placeholder.png")
            cell.name.text = productCollectionViewModel.getProductCollectionData[indexPath.row].productName
            cell.price.text =  productCollectionViewModel.getProductCollectionData[indexPath.row].price
            cell.descriptionData.text = productCollectionViewModel.getProductCollectionData[indexPath.row].descriptionData
            GlobalData.sharedInstance.getImageFromUrl(imageUrl:productCollectionViewModel.getProductCollectionData[indexPath.row].productImage , imageView: cell.imageView)
            cell.wishList_Button.tag = indexPath.row
            cell.compare_Button.tag = indexPath.row
            
            cell.specialPrice.isHidden = true
            
            if productCollectionViewModel.getProductCollectionData[indexPath.row].isInWishlist == true{
                cell.wishList_Button.setImage(UIImage(named: "ic_wishlist_fill")!, for: .normal)
            }else{
                cell.wishList_Button.setImage(UIImage(named: "ic_wishlist_empty")!, for: .normal)
            }
            
            cell.wishList_Button.tag = indexPath.row
            cell.wishList_Button.addTarget(self, action: #selector(addToWishList(sender:)), for: .touchUpInside)
            cell.wishList_Button.isUserInteractionEnabled = true
            
            cell.compare_Button.tag = indexPath.row
            cell.compare_Button.addTarget(self, action: #selector(addToCompare(sender:)), for: .touchUpInside)
            cell.compare_Button.isUserInteractionEnabled = true
            
            if productCollectionViewModel.getProductCollectionData[indexPath.row].typeID == "grouped"{
                cell.price.text =  productCollectionViewModel.getProductCollectionData[indexPath.row].groupedPrice
            }else if productCollectionViewModel.getProductCollectionData[indexPath.row].typeID == "bundle"{
                cell.price.text =  productCollectionViewModel.getProductCollectionData[indexPath.row].formatedMinPrice
            }else{
                if productCollectionViewModel.getProductCollectionData[indexPath.row].isInRange == true{
                    if productCollectionViewModel.getProductCollectionData[indexPath.row].specialPrice < productCollectionViewModel.getProductCollectionData[indexPath.row].normalprice{
                        cell.price.text = productCollectionViewModel.getProductCollectionData[indexPath.row].formatedSpecialPrice
                        let attributeString = NSMutableAttributedString(string: productCollectionViewModel.getProductCollectionData[indexPath.row].formatedPrice)
                        attributeString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 1, range: NSRange(location: 0, length: attributeString.length))
                        cell.specialPrice.attributedText = attributeString
                        cell.specialPrice.isHidden = false
                    }else{
                        cell.price.text =  productCollectionViewModel.getProductCollectionData[indexPath.row].price
                        cell.specialPrice.isHidden = true
                    }
                }
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        let currentCellCount = self.productCollectionView.numberOfItems(inSection: 0)
        if totalCount > currentCellCount{
            return CGSize(width: collectionView.bounds.size.width, height: 55)
        }else{
            return CGSize.zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionFooter {
            let aFooterView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath) as! CustomFooterView
            self.footerView = aFooterView
            self.footerView?.backgroundColor = UIColor.clear
            return aFooterView
        } else {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath)
            return headerView
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        if elementKind == UICollectionElementKindSectionFooter {
            self.footerView?.startAnimate()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
        if elementKind == UICollectionElementKindSectionFooter {
            self.footerView?.stopAnimate()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if change == false{
            return CGSize(width: collectionView.frame.size.width, height:SCREEN_WIDTH/2 + 50)
        }else{
            return CGSize(width: collectionView.frame.size.width/2, height:SCREEN_WIDTH/2.5 + 120)
        }
    }
    
    func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "catalogproduct") as! CatalogProduct
        vc.productName = productCollectionViewModel.getProductCollectionData[indexPath.row].productName
        vc.productId = productCollectionViewModel.getProductCollectionData[indexPath.row].id
        vc.productImageUrl = productCollectionViewModel.getProductCollectionData[indexPath.row].productImage
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
