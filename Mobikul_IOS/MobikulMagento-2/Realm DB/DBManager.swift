//
//  DBManager.swift
//  RealmDatabase
//
//  Created by Webkul on 22/03/18.
//  Copyright © 2018 Webkul. All rights reserved.
//

import Foundation
import RealmSwift
import UIKit
import RealmSwift

class DBManager {
    
    public var database:Realm
    static let sharedInstance = DBManager()
    
    private init() {
        do {
            let data = try! Realm()
            self.database = data
        }
    }
    
    func deleteAllFromDatabase()  {
        do{
            let realm =  try database.write {
                database.deleteAll()
            }
        }catch let error as NSError {
            print("ss", error)
        }
    }
    
    func storeDataToDataBase(data: String, ApiName: String, dataBaseObject: AllDataCollection) {
        let result =  DBManager.sharedInstance.database.object(ofType: AllDataCollection.self, forPrimaryKey: ApiName)
        if let previousData = result {
            try? DBManager.sharedInstance.database.write {
                previousData.data = data
            }
        } else {
            dataBaseObject.data = data
            dataBaseObject.ApiName = ApiName
            try? DBManager.sharedInstance.database.write {
                DBManager.sharedInstance.database.add(dataBaseObject, update: true)
            }
        }
    }
    
    func getJsonDatafromDatabase(ApiName:String , taskCallback: @escaping (Bool,
        AnyObject?) -> Void) {
        var resultData : AnyObject!
        do{
            try DBManager.sharedInstance.database.write {
                let result =  DBManager.sharedInstance.database.object(ofType: AllDataCollection.self, forPrimaryKey: ApiName)
                if result?.data != nil{
                    resultData = GlobalData.sharedInstance.convertToDictionary(text: (result?.data)!) as AnyObject
                }
            }
        } catch {
            print(error)
        }
        
        if resultData == nil {
            taskCallback(false,resultData)
        } else {
            taskCallback(true,resultData)
        }
    }
}

class AllDataCollection: Object {
    
    @objc dynamic var ApiName:String = ""
    @objc dynamic var data:String = ""
    
    override static func primaryKey() -> String? {
        return "ApiName"
    }
}
