//
//  CustomerLogin.swift
//  Magento2V4Theme
//
//  Created by Webkul on 10/02/18.
//  Copyright © 2018 Webkul. All rights reserved.
//

import UIKit


import LocalAuthentication

class CustomerLogin: UIViewController {
    
    @IBOutlet weak var appName: UILabel!
    @IBOutlet weak var emailIdField: UITextField!
    @IBOutlet weak var passwordtextField: HideShowPasswordTextField!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    
    var whichApiToProcess:String = ""
    var emailId:String = ""
    var password:String = ""
    var moveToSignal:String = ""
    var userEmail:String = ""
    var context = LAContext()
    let kMsgShowReason = "🌛 Try to dismiss this screen 🌜"
    var errorMessage:String = ""
    var NotAgainCallTouchId :Bool = false
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let pastelView = PastelView(frame: view.bounds)
        
        // Custom Direction
        pastelView.startPastelPoint = .bottomLeft
        pastelView.endPastelPoint = .topRight
        // Custom Duration
        pastelView.animationDuration = 3.0
        pastelView.setColors([UIColor().HexToColor(hexString: "42E695"),UIColor().HexToColor(hexString: "3BB2B8")])
        pastelView.startAnimation()
        view.insertSubview(pastelView, at: 0)
        
        loginButton.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        loginButton.setTitleColor(UIColor.white, for: .normal)
        loginButton.layer.cornerRadius = 5
        loginButton.layer.masksToBounds = true
        loginButton.layer.borderWidth = 1
        loginButton.layer.borderColor = UIColor.white.cgColor
        
        signupButton.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        signupButton.setTitleColor(UIColor.white, for: .normal)
        signupButton.layer.cornerRadius = 5
        signupButton.layer.masksToBounds = true
        signupButton.layer.borderWidth = 1
        signupButton.layer.borderColor = UIColor.white.cgColor
        
        loginButton.setTitle(GlobalData.sharedInstance.language(key: "login"), for: .normal)
        forgotPasswordButton.setTitleColor(UIColor.white, for: .normal)
        forgotPasswordButton.setTitle(GlobalData.sharedInstance.language(key: "forgotpassword"), for: .normal)
        signupButton.setTitle(GlobalData.sharedInstance.language(key: "signup"), for: .normal)
        
        emailIdField.setLeftPaddingPoints(10)
        emailIdField.setRightPaddingPoints(10)
        emailIdField.placeholder = GlobalData.sharedInstance.language(key: "email")
        emailIdField.layer.borderColor = UIColor.white.cgColor
        emailIdField.layer.borderWidth = 1.0
        emailIdField.layer.cornerRadius = 5
        
        passwordtextField.placeholder = GlobalData.sharedInstance.language(key: "password")
        passwordtextField.layer.borderColor = UIColor.white.cgColor
        passwordtextField.layer.borderWidth = 1.0
        passwordtextField.layer.cornerRadius = 5
        passwordtextField.layer.masksToBounds = true
        
        if defaults.object(forKey: "touchIdFlag") == nil{
            defaults.set("0", forKey: "touchIdFlag")
            defaults.synchronize()
        }
        
        context = LAContext()
        appName.text = GlobalData.sharedInstance.language(key: "applicationname")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(self.moveToSignal != "cart"){
            if defaults.object(forKey: "touchIdFlag") as! String == "1"{
                let  AC = UIAlertController(title: GlobalData.sharedInstance.language(key: "alert"), message: GlobalData.sharedInstance.language(key: "loginbytouchid"), preferredStyle: .alert)
                let okBtn = UIAlertAction(title: GlobalData.sharedInstance.language(key: "yes"), style:.default, handler: {(_ action: UIAlertAction) -> Void in
                    self.configureTouchIdBeforeLogin()
                })
                let cancelBtn = UIAlertAction(title: GlobalData.sharedInstance.language(key: "no"), style:.destructive, handler: {(_ action: UIAlertAction) -> Void in
                    
                })
                AC.addAction(okBtn)
                AC.addAction(cancelBtn)
                self.parent!.present(AC, animated: true, completion: {  })
            }
        }
    }
    
    @IBAction func clickOnBack(_ sender: Any) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.popViewController(animated: true)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func callingHttppApi(){
        GlobalData.sharedInstance.showLoader()
        self.view.isUserInteractionEnabled = false
        var requstParams = [String:Any]()
        if defaults.object(forKey: "storeId") != nil{
            requstParams["storeId"] = defaults.object(forKey: "storeId") as! String
        }
        let quoteId = defaults.object(forKey:"quoteId")
        let deviceToken = defaults.object(forKey:"deviceToken")
        if(quoteId != nil){
            requstParams["quoteId"] = quoteId
        }
        if deviceToken != nil{
            requstParams["token"] = deviceToken
        }
        
        if(whichApiToProcess == "forgotpassword"){
            requstParams["email"] = self.userEmail
            GlobalData.sharedInstance.callingHttpRequest(params:requstParams, apiname:"mobikulhttp/customer/forgotpassword", currentView: self){success,responseObject in
                if success == 1{
                    if responseObject?.object(forKey: "storeId") != nil{
                        let storeId:String = String(format: "%@", responseObject!.object(forKey: "storeId") as! CVarArg)
                        if storeId != "0"{
                            defaults .set(storeId, forKey: "storeId")
                        }
                    }
                    
                    GlobalData.sharedInstance.dismissLoader()
                    self.view.isUserInteractionEnabled = true
                    let dict  = JSON(responseObject as! NSDictionary)
                    print(dict)
                    if dict["success"].boolValue == true{
                        GlobalData.sharedInstance.showSuccessSnackBar(msg: dict["message"].stringValue)
                    }else{
                        GlobalData.sharedInstance.showErrorSnackBar(msg: dict["message"].stringValue)
                    }
                }else if success == 2{
                    self.callingHttppApi()
                    GlobalData.sharedInstance.dismissLoader()
                }
            }
        }else{
            requstParams["username"] = emailIdField.text
            requstParams["password"] = passwordtextField.text
            requstParams["websiteId"] = DEFAULT_WEBSITE_ID
            let width = String(format:"%f", SCREEN_WIDTH * UIScreen.main.scale)
            requstParams["width"] = width
            GlobalData.sharedInstance.callingHttpRequest(params:requstParams, apiname:"mobikulhttp/customer/logIn", currentView: self){success,responseObject in
                if success == 1{
                    if responseObject?.object(forKey: "storeId") != nil{
                        let storeId:String = String(format: "%@", responseObject!.object(forKey: "storeId") as! CVarArg)
                        if storeId != "0"{
                            defaults .set(storeId, forKey: "storeId")
                        }
                    }
                    self.view.isUserInteractionEnabled = true
                    self.doFurtherProcessingWithResult(data:responseObject!)
                }else if success == 2{
                    GlobalData.sharedInstance.dismissLoader()
                    self.callingHttppApi()
                }
            }
        }
    }
    
    func doFurtherProcessingWithResult(data:AnyObject){
        GlobalData.sharedInstance.dismissLoader()
        print(data)
        let responseData = data as! NSDictionary
        if responseData.object(forKey: "success") as! Bool == true{
            defaults.set(responseData.object(forKey: "customerEmail"), forKey: "customerEmail")
            defaults.set(responseData.object(forKey: "customerToken"), forKey: "customerId")
            defaults.set(responseData.object(forKey: "customerName"), forKey: "customerName")
            UserDefaults.standard.removeObject(forKey: "quoteId")
            defaults.synchronize()
            if(defaults.object(forKey: "quoteId") != nil){
                defaults.set(nil, forKey: "quoteId")
                defaults.synchronize()
            }
            let profileImage = (data.object(forKey: "profileImage") as? String!)!
            let bannerImage  = (data.object(forKey: "bannerImage") as? String!)!
            
            if profileImage != ""{
                defaults.set(profileImage, forKey: "profilePicture")
            }
            if bannerImage != ""{
                defaults.set(bannerImage, forKey: "profileBanner")
            }
            let cartTotalDict = JSON(data)
            if data.object(forKey: "cartCount") != nil{
                let cartCount  = cartTotalDict["cartCount"].intValue
                if cartCount > 0{
                    self.tabBarController!.tabBar.items?[3].badgeValue = cartTotalDict["cartCount"].stringValue
                }
            }
            
            defaults.synchronize()
            
            if defaults.object(forKey: "touchIdFlag") != nil && self.NotAgainCallTouchId == false{
                if defaults.object(forKey: "touchIdFlag") as! String == "0"{
                    let  AC = UIAlertController(title: GlobalData.sharedInstance.language(key: "alert"), message: GlobalData.sharedInstance.language(key: "wouldyouliketoconnectappwithtouchid"), preferredStyle: .alert)
                    let okBtn = UIAlertAction(title: GlobalData.sharedInstance.language(key: "yes"), style:.default, handler: {(_ action: UIAlertAction) -> Void in
                        self.configureTouchIdafterLogin()
                    })
                    let cancelBtn = UIAlertAction(title: GlobalData.sharedInstance.language(key: "no"), style: .destructive, handler: {(_ action: UIAlertAction) -> Void in
                        defaults.set("0", forKey: "touchIdFlag")
                        defaults.synchronize()
                        self.tabBarController?.tabBar.isHidden = false
                        self.navigationController?.popViewController(animated: true)
                    })
                    AC.addAction(okBtn)
                    AC.addAction(cancelBtn)
                    self.parent!.present(AC, animated: true, completion: {  })
                    
                }else if defaults.object(forKey: "touchIdFlag") as! String == "1" {
                    let  AC = UIAlertController(title: GlobalData.sharedInstance.language(key: "alert"), message: GlobalData.sharedInstance.language(key: "wouldyouliketoreset"), preferredStyle: .alert)
                    let okBtn = UIAlertAction(title: "yes".localized, style:.default, handler: {(_ action: UIAlertAction) -> Void in
                        self.configureTouchIdafterLogin()
                    })
                    let cancelBtn = UIAlertAction(title: "no".localized, style: .destructive, handler: {(_ action: UIAlertAction) -> Void in
                        defaults.set("1", forKey: "touchIdFlag")
                        defaults.synchronize()
                        self.tabBarController?.tabBar.isHidden = false
                        self.navigationController?.popViewController(animated: true)
                    })
                    AC.addAction(okBtn)
                    AC.addAction(cancelBtn)
                    self.parent!.present(AC, animated: true, completion: {  })
                }else{
                    self.tabBarController?.tabBar.isHidden = false
                    self.navigationController?.popViewController(animated: true)
                }
            }else{
                self.tabBarController?.tabBar.isHidden = false
                self.navigationController?.popViewController(animated: true)
            }
        }else{
            GlobalData.sharedInstance.showErrorSnackBar(msg: responseData.object(forKey: "message") as! String)
        }
    }
    
    @IBAction func signupClick(_ sender: UIButton) {
        self.performSegue(withIdentifier: "createaccount", sender: self)
    }
    
    @IBAction func forgaotPasswordClick(_ sender: Any) {
        let AC = UIAlertController(title:GlobalData.sharedInstance.language(key:"enteremail"), message: "", preferredStyle: .alert)
        AC.addTextField { (textField) in
            textField.placeholder = GlobalData.sharedInstance.language(key:"enteremail")
        }
        let okBtn = UIAlertAction(title: GlobalData.sharedInstance.language(key:"yes"), style: .default, handler: {(_ action: UIAlertAction) -> Void in
            let textField = AC.textFields![0]
            if((textField.text?.characters.count)! < 1){
                GlobalData.sharedInstance.showErrorSnackBar(msg:GlobalData.sharedInstance.language(key:"pleasefillemailid") )
            }else if  !GlobalData.sharedInstance.checkValidEmail(data: textField.text!){
                GlobalData.sharedInstance.showErrorSnackBar(msg:GlobalData.sharedInstance.language(key: "pleaseentervalidemail"))
            }else{
                self.userEmail = textField.text!
                self.whichApiToProcess = "forgotpassword"
                self.callingHttppApi()
            }
        })
        let noBtn = UIAlertAction(title: GlobalData.sharedInstance.language(key:"no"), style:.destructive, handler: {(_ action: UIAlertAction) -> Void in
        })
        AC.addAction(okBtn)
        AC.addAction(noBtn)
        self.present(AC, animated: true, completion: {  })
    }
    
    @IBAction func LoginClick(_ sender: UIButton) {
        view.endEditing(true)
        emailId = emailIdField.text!
        password = passwordtextField.text!
        var isValid = 0
        var errorMessage = ""
        if !GlobalData.sharedInstance.checkValidEmail(data: emailId){
            isValid = 1
            errorMessage = GlobalData.sharedInstance.language(key: "pleaseentervalidemail")
        }else if password == ""{
            isValid = 1
            errorMessage = GlobalData.sharedInstance.language(key: "enterpassword")
        }
        if isValid == 1{
            GlobalData.sharedInstance.showErrorSnackBar(msg: errorMessage)
        }else{
            whichApiToProcess = ""
            callingHttppApi()
        }
    }
    
    //MARK:- Touch ID
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////   After Login we are setting /////////////////////////////////////////////////////////////////////////////////
    
    func configureTouchIdafterLogin(){
        var policy: LAPolicy?
        if #available(iOS 9.0, *) {
            // iOS 9+ users with Biometric and Passcode verification
            policy = .deviceOwnerAuthentication
        } else {
            // iOS 8+ users with Biometric and Custom (Fallback button) verification
            context.localizedFallbackTitle = ""
            policy = .deviceOwnerAuthenticationWithBiometrics
        }
        var err: NSError?
        guard context.canEvaluatePolicy(policy!, error: &err) else {
            print("sfsf")
            
            //Touch Id not set in device
            let  AC = UIAlertController(title: "Error", message: GlobalData.sharedInstance.language(key: "touchidisnotenabledinyourdevice"), preferredStyle: .alert)
            let okBtn = UIAlertAction(title: "OK", style:.default, handler: {(_ action: UIAlertAction) -> Void in
                self.goToDashBoardWithoutTouchId()
            })
            AC.addAction(okBtn)
            self.parent!.present(AC, animated: true, completion: {  })
            return
        }
        
        loginProcessAfterLogin(policy: policy!)
    }
    
    private func loginProcessAfterLogin(policy: LAPolicy) {
        // Start evaluation process with a callback that is executed when the user ends the process successfully or not
        context.evaluatePolicy(policy, localizedReason: kMsgShowReason, reply: { (success, error) in
            DispatchQueue.main.async {
                
                guard success else {
                    guard let error = error else {
                        self.showUnexpectedErrorMessageAfterLogin()
                        return
                    }
                    switch(error) {
                    case LAError.authenticationFailed:
                        self.errorMessage = GlobalData.sharedInstance.language(key: "therewasaproblemverifyingyouridentity")
                    case LAError.userCancel:
                        self.errorMessage = GlobalData.sharedInstance.language(key: "authenticationwascanceledbyuser")
                    default:
                        self.errorMessage = GlobalData.sharedInstance.language(key: "touchidmaynotbeconfigured")
                        break
                    }
                    
                    let  AC = UIAlertController(title: GlobalData.sharedInstance.language(key: "error"), message: self.errorMessage, preferredStyle: .alert)
                    let okBtn = UIAlertAction(title: GlobalData.sharedInstance.language(key: "ok"), style:.default, handler: {(_ action: UIAlertAction) -> Void in
                        self.goToDashBoardWithoutTouchId()
                    })
                    AC.addAction(okBtn)
                    self.parent!.present(AC, animated: true, completion: {  })
                    return
                }
                // Good news! Everything went fine 👏
                self.goToDashBoardWithTouchId()
            }
        })
    }
    
    func goToDashBoardWithoutTouchId(){
        defaults.set("0", forKey: "touchIdFlag")
        defaults.synchronize()
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
    func goToDashBoardWithTouchId(){
        defaults.set("1", forKey: "touchIdFlag")
        defaults.set(emailId, forKey: "TouchEmailId")
        defaults.set(password, forKey: "TouchPasswordValue")
        defaults.synchronize()
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
    func showUnexpectedErrorMessageAfterLogin(){
        let  AC = UIAlertController(title: GlobalData.sharedInstance.language(key: "error"), message: GlobalData.sharedInstance.language(key: "erroroccured"), preferredStyle: .alert)
        let okBtn = UIAlertAction(title: GlobalData.sharedInstance.language(key: "ok"), style:.default, handler: {(_ action: UIAlertAction) -> Void in
            self.tabBarController?.tabBar.isHidden = false
            self.navigationController?.popViewController(animated: true)
        })
        AC.addAction(okBtn)
        self.parent!.present(AC, animated: true, completion: {  })
    }
    
    /////////////////////   before Login we are setting /////////////////////////////////////////////////////////////////////////////////
    func configureTouchIdBeforeLogin(){
        var policy: LAPolicy?
        policy = .deviceOwnerAuthenticationWithBiometrics
        context.localizedFallbackTitle = ""
        
        var err: NSError?
        guard context.canEvaluatePolicy(policy!, error: &err) else {
            return
        }
        loginProcessBeforeLogin(policy: policy!)
    }
    
    private func loginProcessBeforeLogin(policy: LAPolicy) {
        // Start evaluation process with a callback that is executed when the user ends the process successfully or not
        context.evaluatePolicy(policy, localizedReason: kMsgShowReason, reply: { (success, error) in
            DispatchQueue.main.async {
                
                guard success else {
                    guard let error = error else {
                        self.showUnexpectedErrorMessageBeforeLogin()
                        return
                    }
                    switch(error) {
                    case LAError.authenticationFailed:
                        self.errorMessage = GlobalData.sharedInstance.language(key: "therewasaproblemverifyingyouridentity")
                    case LAError.userCancel:
                        self.errorMessage = GlobalData.sharedInstance.language(key: "authenticationwascanceledbyuser")
                        
                    default:
                        self.errorMessage = GlobalData.sharedInstance.language(key: "touchidmaynotbeconfigured")
                        break
                    }
                    let  AC = UIAlertController(title: GlobalData.sharedInstance.language(key: "error"), message: self.errorMessage, preferredStyle: .alert)
                    let okBtn = UIAlertAction(title: GlobalData.sharedInstance.language(key: "ok"), style:.default, handler: {(_ action: UIAlertAction) -> Void in
                        
                    })
                    AC.addAction(okBtn)
                    self.parent!.present(AC, animated: true, completion: {  })
                    return
                }
                // Good news! Everything went fine 👏
                self.callApiWithSavedCredential()
            }
        })
    }
    
    func showUnexpectedErrorMessageBeforeLogin(){
        let  AC = UIAlertController(title: GlobalData.sharedInstance.language(key: "error"), message: GlobalData.sharedInstance.language(key: "erroroccured"), preferredStyle: .alert)
        let okBtn = UIAlertAction(title: GlobalData.sharedInstance.language(key: "ok"), style:.default, handler: {(_ action: UIAlertAction) -> Void in
            
        })
        AC.addAction(okBtn)
        self.parent!.present(AC, animated: true, completion: {  })
    }
    
    func callApiWithSavedCredential(){
        emailId = defaults.object(forKey: "TouchEmailId") as! String
        password = defaults.object(forKey: "TouchPasswordValue") as! String
        emailIdField.text = defaults.object(forKey: "TouchEmailId") as? String
        passwordtextField.text = defaults.object(forKey: "TouchPasswordValue") as? String
        NotAgainCallTouchId = true
        whichApiToProcess = ""
        callingHttppApi()
    }
}
