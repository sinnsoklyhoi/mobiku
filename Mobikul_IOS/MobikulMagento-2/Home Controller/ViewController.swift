//
//  ViewController.swift
//  Magento2V4Theme
//
//  Created by Webkul on 07/02/18.
//  Copyright © 2018 Webkul. All rights reserved.
//

import UIKit
import SwiftMessages

class ViewController: BaseVC,productViewControllerHandlerDelegate,bannerViewControllerHandlerDelegate,CategoryViewControllerHandlerDelegate,hotDealProductViewControllerHandlerDelegate,RecentProductViewControllerHandlerDelegate,UISearchBarDelegate ,UITabBarControllerDelegate,UIViewControllerPreviewingDelegate,PreviewControllerDelegate{
    
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var homeViewModel : HomeViewModel!
    var productName:String = ""
    var productId:String = ""
    var productImage:String = ""
    var categoryId:String = ""
    var categoryName:String = ""
    var categoryType:String = ""
    var whichApiToProcess:String = ""
    var launchView:UIViewController!
    var refreshControl:UIRefreshControl!
    var responseObject : AnyObject!
    var dataBaseObject:AllDataCollection!
    var deepLinkUserInfo = [String: Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
        ThemeManager.applyTheme(bar:(self.navigationController?.navigationBar)!)
        
        let launchScreen = UIStoryboard(name: "LaunchScreen", bundle: nil)
        launchView = launchScreen.instantiateInitialViewController()
        self.view.addSubview(launchView!.view)
        homeViewModel = HomeViewModel()
        dataBaseObject = AllDataCollection()
        homeTableView?.register(BannerTableViewCell.nib, forCellReuseIdentifier: BannerTableViewCell.identifier)
        homeTableView?.register(TopCategoryTableViewCell.nib, forCellReuseIdentifier: TopCategoryTableViewCell.identifier)
        homeTableView?.register(ProductTableViewCell.nib, forCellReuseIdentifier: ProductTableViewCell.identifier)
        homeTableView?.register(HotdealsTableViewCell.nib, forCellReuseIdentifier: HotdealsTableViewCell.identifier)
        homeTableView?.register(RecentViewTableViewCell.nib, forCellReuseIdentifier: RecentViewTableViewCell.identifier)
        homeTableView?.register(ShopByCategoriesViewCell.nib, forCellReuseIdentifier: ShopByCategoriesViewCell.identifier)
        homeTableView?.register(NewArriverViewCell.nib, forCellReuseIdentifier: NewArriverViewCell.identifier)
        homeTableView?.register(PromotionViewCell.nib, forCellReuseIdentifier: PromotionViewCell.identifier)
        homeTableView?.register(DealOfWeekViewCell.nib, forCellReuseIdentifier: DealOfWeekViewCell.identifier)
        
//        self.navigationItem.title = GlobalData.sharedInstance.language(key: "applicationname")
        self.homeTableView?.dataSource = homeViewModel
        self.homeTableView?.delegate = homeViewModel
        GlobalVariables.hometableView = homeTableView
        self.homeViewModel.homeViewController = self
        self.homeTableView.separatorColor = UIColor.clear
        searchBar.delegate = self
        callingHttppApi();
        searchBar.placeholder = GlobalData.sharedInstance.language(key: "searchentirestore")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.pushNotificationReceivedCategoryTap), name: NSNotification.Name(rawValue: "pushNotificationforCategoryOnTap"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.pushNotificationReceivedProductTap), name: NSNotification.Name(rawValue: "pushNotificationforProductOnTap"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.pushNotificationReceivedCustomCollectionTap), name: NSNotification.Name(rawValue: "pushNotificationforCustomCollectionOnTap"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshPageRecentView), name: NSNotification.Name(rawValue: "refreshRecentView"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.pushNotificationReceivedOtherTap), name: NSNotification.Name(rawValue: "pushNotificationforOtherOnTap"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.deepLinking), name: NSNotification.Name(rawValue: "deepLink"), object: nil)
        
        self.tabBarController?.delegate = self
        refreshControl = UIRefreshControl()
        let attributes = [NSAttributedStringKey.foregroundColor: UIColor.black]
        let attributedTitle = NSAttributedString(string: GlobalData.sharedInstance.language(key: "refreshing"), attributes: attributes)
        refreshControl.attributedTitle = attributedTitle
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            homeTableView.refreshControl = refreshControl
        } else {
            homeTableView.backgroundView = refreshControl
        }
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        if ((self.presentedViewController?.isKind(of: PreviewViewController.self)) != nil) {
            return nil
        }
        
        var productDescription:String = ""
        productImage = ""
        var requiredOption:Int = 0
        
        
        
        if let currentRow =  (previewingContext.sourceView as? UICollectionView)?.indexPathForItem(at: location)?.row{
            if GlobalVariables.showFeature{
                if currentRow < self.homeViewModel.featuredProductCollectionModel.count{
                   productDescription = self.homeViewModel.featuredProductCollectionModel[currentRow].shortDescription
                   productImage = self.homeViewModel.featuredProductCollectionModel[currentRow].image
                   productId = self.homeViewModel.featuredProductCollectionModel[currentRow].productID
                   requiredOption = self.homeViewModel.featuredProductCollectionModel[currentRow].requiredOptions
                  self.productName = self.homeViewModel.featuredProductCollectionModel[currentRow].name
                }else{
                    return nil
                }
                
            }else{
                if currentRow < self.homeViewModel.letestProductCollectionModel.count{
                  productDescription = self.homeViewModel.letestProductCollectionModel[currentRow].shortDescription
                  productImage = self.homeViewModel.letestProductCollectionModel[currentRow].image
                  productId = self.homeViewModel.letestProductCollectionModel[currentRow].productID
                  requiredOption = self.homeViewModel.letestProductCollectionModel[currentRow].requiredOptions
                  self.productName = self.homeViewModel.letestProductCollectionModel[currentRow].name
                }else{
                    return nil
                }
            }
            
        }
 
        // PEEK (shallow press): return the preview view controller here
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let previewView = storyboard.instantiateViewController(withIdentifier: "PreviewView") as? PreviewViewController
        previewView?.productDescription = productDescription
        previewView?.imageUrl = productImage
        previewView?.delegate = self
        previewView?.requiredOptions = requiredOption
        
        return previewView
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {

        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let catalogProduct = storyboard.instantiateViewController(withIdentifier: "catalogproduct") as? CatalogProduct
        catalogProduct?.productName = self.productName
        catalogProduct?.productImageUrl = self.productImage
        catalogProduct?.productId = self.productId
        self.navigationController?.pushViewController(catalogProduct!, animated: true)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        
        // called when the interface environment changes
        // one of those occasions would be if the user enables/disables 3D Touch
        // so we'll simply check again at this point
    }
    
    func previewAddToCart(){
        whichApiToProcess = "addtocart";
        self.callingExtraHttpApi()
    }
    
    func previewShare(){
        let productUrl = productImage
        let activityItems = [productUrl]
        let activityController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        if UI_USER_INTERFACE_IDIOM() == .phone {
            self.present(activityController, animated: true, completion: {  })
        }else {
            let popup = UIPopoverController(contentViewController: activityController)
            popup.present(from: CGRect(x: CGFloat(self.view.frame.size.width / 2), y: CGFloat(self.view.frame.size.height / 4), width: CGFloat(0), height: CGFloat(0)), in: self.view, permittedArrowDirections: .any, animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.isUserInteractionEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
    }
    
    //Refresh page for Recent View
    @objc func refreshPageRecentView()    {
        print(homeTableView.numberOfSections)
        let productModel = ProductViewModel()
        print(productModel.getProductDataFromDB())
        
        if responseObject != nil {
            self.homeViewModel.getData(data: responseObject!, recentViewData : productModel.getProductDataFromDB()) {
                (data : Bool) in
                if data {
                    self.homeTableView.reloadData()
                }
            }
        }
    }
    
    @objc func refresh(_ refreshControl: UIRefreshControl) {
        callingHttppApi()
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let tabitem: Int = tabBarController.selectedIndex
        let navigation:UINavigationController = tabBarController.viewControllers?[tabitem] as! UINavigationController
        navigation.popToRootViewController(animated: true)
    }
    
    func tabBarController(_ tabBarController: UITabBarController,
                          shouldSelect viewController: UIViewController) -> Bool {
        //let tabitem: Int = tabBarController.selectedIndex
        GlobalData.sharedInstance.removePreviousNetworkCall()
        GlobalData.sharedInstance.dismissLoader()
        self.refreshControl.endRefreshing()
        self.navigationController?.isNavigationBarHidden = false
        return true
    }
    
    @objc func pushNotificationReceivedCategoryTap(_ note: Notification) {
        var root  = note.userInfo
        categoryId = root?["categoryId"] as! String
        categoryName = root?["categoryName"] as! String
        categoryType = ""
        self.performSegue(withIdentifier: "productcategory", sender: self)
    }
    
    @objc func pushNotificationReceivedProductTap(_ note: Notification) {
        var root = note.userInfo
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "catalogproduct") as! CatalogProduct
        vc.productName = root?["productName"] as! String
        vc.productId = root?["productId"] as! String
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func pushNotificationReceivedCustomCollectionTap(_ note: Notification) {
        var root = note.userInfo;
        categoryId = root?["id"] as! String
        categoryName = root?["title"] as! String
        categoryType = "custom"
        self.performSegue(withIdentifier: "productcategory", sender: self)
        
    }
    
    @objc func pushNotificationReceivedOtherTap(_ note: Notification) {
        var root = note.userInfo;
        let title = root?["title"] as! String
        let content = root?["message"] as! String
        let AC = UIAlertController(title: title, message: content, preferredStyle: .alert)
        let okBtn = UIAlertAction(title: GlobalData.sharedInstance.language(key: "ok"), style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.callingHttppApi();
        })
        AC.addAction(okBtn)
        self.parent!.present(AC, animated: true, completion: { })
    }
    
    @objc func deepLinking(_ userInfo : Notification){
        let root = userInfo.userInfo as! [String: Any]
        print(root)
        deepLinkUserInfo = root
        Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(callDeepLinking), userInfo: root, repeats: false)
    }
    
    @objc func callDeepLinking() {
        whichApiToProcess = "deepLink"
        callingExtraHttpApi()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        view.endEditing(true)
        self.performSegue(withIdentifier: "search", sender: self)
    }
    
    @IBAction func showNotificationClick(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "notification", sender: self)
    }
    
    func callingHttppApi() {
        DispatchQueue.main.async {
            self.view.isUserInteractionEnabled = false
            var requstParams = [String:Any]();
            if defaults.object(forKey: "storeId") != nil{
                requstParams["storeId"] = defaults.object(forKey: "storeId") as! String
            }
            let customerId = defaults.object(forKey:"customerId");
            if customerId != nil{
                requstParams["customerToken"] = customerId
            }
            
            if defaults.object(forKey: "currency") != nil{
                requstParams["currency"] = defaults.object(forKey: "currency") as! String
            }
            requstParams["websiteId"] = "1"
            let width = String(format:"%f", SCREEN_WIDTH * UIScreen.main.scale)
            requstParams["width"] = width
            let apiName = "mobikulhttp/catalog/homePageData"
            GlobalData.sharedInstance.callingHttpRequest(params:requstParams, apiname:apiName, currentView: self){success,responseObject in
                if success == 1 {
                    print(responseObject as! NSDictionary)
                    self.refreshControl.endRefreshing()
                    if responseObject?.object(forKey: "storeId") != nil{
                        let storeId:String = String(format: "%@", responseObject!.object(forKey: "storeId") as! CVarArg)
                        if storeId != "0" {
                            defaults .set(storeId, forKey: "storeId")
                        }
                    }
                    if responseObject?.object(forKey: "defaultCurrency") != nil {
                        if defaults.object(forKey: "currency") == nil {
                            defaults .set(responseObject!.object(forKey: "defaultCurrency") as! String, forKey: "currency")
                        }
                    }
                    
                    let dict =  JSON(responseObject as! NSDictionary)
                    self.view.isUserInteractionEnabled = true
                    
                    if dict["success"].boolValue == true {
                        self.responseObject = responseObject!
                        let productModel = ProductViewModel()
                        self.homeViewModel.getData(data: responseObject!, recentViewData : productModel.getProductDataFromDB()) {
                            (data : Bool) in
                            if data {
                                self.homeTableView.reloadDataWithAutoSizingCellWorkAround()
                                self.navigationController?.isNavigationBarHidden = false
                                self.tabBarController?.tabBar.isHidden = false
                                UIView.animate(withDuration: 0.5, animations: {
                                    self.launchView?.view.alpha = 0.0
                                }) { _ in
                                    self.launchView!.view.removeFromSuperview()
                                }
                                
                                // store the data to data base
                                if let data = GlobalData.sharedInstance.json(from: responseObject as! NSDictionary) {
                                    DBManager.sharedInstance.storeDataToDataBase(data: data, ApiName: "mobikulhttp/catalog/homePageData", dataBaseObject: self.dataBaseObject)
                                }
                            }
                        }
                    }
                } else if success == 2 {
                    self.refreshControl.endRefreshing()
                    GlobalData.sharedInstance.dismissLoader()
                    self.callingHttppApi()
                } else if success == 3 {
                    let productModel = ProductViewModel()
                    self.responseObject = responseObject!
                    self.homeViewModel.getData(data: responseObject!, recentViewData: productModel.getProductDataFromDB()) {
                        (data : Bool) in
                        if data {
                            self.homeTableView.reloadDataWithAutoSizingCellWorkAround()
                            self.navigationController?.isNavigationBarHidden = false
                            self.tabBarController?.tabBar.isHidden = false
                            UIView.animate(withDuration: 0.5, animations: {
                                self.launchView?.view.alpha = 0.0
                            }) { _ in
                                self.launchView!.view.removeFromSuperview()
                            }
                        }
                    }
                } else if success == 0 {
                    if let data = responseObject as? NSDictionary   {
                        if let otherError = data.object(forKey: "otherError") as? String  , otherError == "customerNotExist" {
                            print("customerNotExist call")
                            self.refreshControl.endRefreshing()
                            GlobalData.sharedInstance.dismissLoader()
                            defaults.removeObject(forKey: "customerId")
                            self.callingHttppApi()
                        }
                    }
                }
            }
        }
    }
    
    func productClick(name:String,image:String,id:String) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "catalogproduct") as! CatalogProduct
        vc.productImageUrl = image
        vc.productName = name
        vc.productId = id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func bannerProductClick(type:String,image:String,id:String,title:String){
        if type == "category"{
            categoryId = id
            categoryName = title
            categoryType = ""
            self.performSegue(withIdentifier: "productcategory", sender: self)
        }else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "catalogproduct") as! CatalogProduct
            vc.productImageUrl = image
            vc.productName = title
            vc.productId = id
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func categoryProductClick(name:String,ID:String){
        categoryId = ID
        categoryName = name
        categoryType = ""
        self.performSegue(withIdentifier: "productcategory", sender: self)
    }
    
    func newAndFeartureAddToWishList(productID:String){
        let customerId = defaults.object(forKey:"customerId");
        if customerId != nil{
            self.productId = productID;
            whichApiToProcess = "addtowishlist"
            callingExtraHttpApi()
        }else{
            let alertView = UIAlertController(title: "loginrequired".localized, message: "doyouwanttologin".localized, preferredStyle: .alert)
            let loginBtn = UIAlertAction(title: "loginalert".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "CustomerLogin") as? CustomerLogin
                self.navigationController?.pushViewController(vc!, animated: true)
            })
            let cancelBtn = UIAlertAction(title: "cancel".localized, style: .destructive, handler: {(_ action: UIAlertAction) -> Void in
            })
            
            alertView.addAction(cancelBtn)
            alertView.addAction(loginBtn)
            self.present(alertView, animated: true, completion: nil)
        }
    }
    
    func newAndFeartureAddToCompare(productID:String){
        self.productId = productID;
        whichApiToProcess = "addtocompare"
        callingExtraHttpApi()
    }
    
    func viewAllClick(type:String) {
        if type == "new"{
            categoryType = "newproduct"
            categoryName = "newproducts".localized
            categoryId = ""
        }else{
            categoryType = "featureproduct"
            categoryName = "featuredproducts".localized
            categoryId = ""
        }
        self.performSegue(withIdentifier: "productcategory", sender: self)
    }
    
    func hotDealProductClick(name: String, image: String, id: String) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "catalogproduct") as! CatalogProduct
        vc.productImageUrl = image
        vc.productName = name
        vc.productId = id
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func hotDealAddToWishList(productID:String){
        let customerId = defaults.object(forKey:"customerId");
        if customerId != nil{
            self.productId = productID;
            whichApiToProcess = "addtowishlist"
            callingExtraHttpApi()
        }else{
            let alertView = UIAlertController(title: "loginrequired".localized, message: "doyouwanttologin".localized, preferredStyle: .alert)
            let loginBtn = UIAlertAction(title: "loginalert".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "CustomerLogin") as? CustomerLogin
                self.navigationController?.pushViewController(vc!, animated: true)
            })
            let cancelBtn = UIAlertAction(title: "cancel".localized, style: .destructive, handler: {(_ action: UIAlertAction) -> Void in
            })
            
            alertView.addAction(cancelBtn)
            alertView.addAction(loginBtn)
            self.present(alertView, animated: true, completion: nil)
        }
    }
    
    func hotDealAddToCompare(productID:String){
        self.productId = productID;
        whichApiToProcess = "addtocompare"
        callingExtraHttpApi()
    }
    
    func hotDealViewAllClick(){
        categoryType = "hotdeal"
        categoryName = "hotdealproduct".localized
        categoryId = ""
        self.performSegue(withIdentifier: "productcategory", sender: self)
    }
    
    func callingExtraHttpApi(){
        self.view.isUserInteractionEnabled = false
        GlobalData.sharedInstance.showLoader()
        var requstParams = [String:Any]();
        if defaults.object(forKey: "storeId") != nil{
            requstParams["storeId"] = defaults.object(forKey: "storeId") as! String
        }
        let customerId = defaults.object(forKey:"customerId")
        if customerId != nil{
            requstParams["customerToken"] = customerId
        }
        let quoteId = defaults.object(forKey:"quoteId")
        if(quoteId != nil){
            requstParams["quoteId"] = quoteId
        }
        
        if whichApiToProcess == "addtowishlist"{
            requstParams["productId"] = productId
            GlobalData.sharedInstance.callingHttpRequest(params:requstParams, apiname:"mobikulhttp/catalog/addtoWishlist", currentView: self){success,responseObject in
                if success == 1{
                    if responseObject?.object(forKey: "storeId") != nil{
                        let storeId:String = String(format: "%@", responseObject!.object(forKey: "storeId") as! CVarArg)
                        if storeId != "0"{
                            defaults .set(storeId, forKey: "storeId")
                        }
                    }
                    
                    GlobalData.sharedInstance.dismissLoader()
                    self.view.isUserInteractionEnabled = true
                    let data = responseObject as! NSDictionary
                    let errorCode: Bool = data .object(forKey:"success") as! Bool
                    
                    if errorCode == true{
                        GlobalData.sharedInstance.showSuccessSnackBar(msg:GlobalData.sharedInstance.language(key: "successwishlist"))
                    }
                    else{
                        GlobalData.sharedInstance.showErrorSnackBar(msg:GlobalData.sharedInstance.language(key: "errorwishlist"))
                    }
                    
                }else if success == 2{
                    GlobalData.sharedInstance.dismissLoader()
                    self.callingExtraHttpApi()
                }
            }
        }else if whichApiToProcess == "addtocompare"{
            requstParams["productId"] = productId
            GlobalData.sharedInstance.callingHttpRequest(params:requstParams, apiname:"mobikulhttp/catalog/addtocompare", currentView: self){success,responseObject in
                if success == 1{
                    if responseObject?.object(forKey: "storeId") != nil{
                        let storeId:String = String(format: "%@", responseObject!.object(forKey: "storeId") as! CVarArg)
                        if storeId != "0"{
                            defaults .set(storeId, forKey: "storeId")
                        }
                    }
                    
                    GlobalData.sharedInstance.dismissLoader()
                    self.view.isUserInteractionEnabled = true
                    let data = responseObject as! NSDictionary
                    let errorCode: Bool = data .object(forKey:"success") as! Bool
                    
                    if errorCode == true{
                        self.showSuccessMessgae(data:data.object(forKey: "message") as! String)
                    }else{
                        GlobalData.sharedInstance.showErrorSnackBar(msg:data.object(forKey: "message") as! String)
                    }
                }else if success == 2{
                    GlobalData.sharedInstance.dismissLoader()
                    self.callingExtraHttpApi()
                }
            }
        }else if whichApiToProcess == "addtocart"{
            requstParams["productId"] = productId
            GlobalData.sharedInstance.callingHttpRequest(params:requstParams, apiname:"mobikulhttp/checkout/addtoCart", currentView: self){success,responseObject in
                if success == 1{
                    if responseObject?.object(forKey: "storeId") != nil{
                        let storeId:String = String(format: "%@", responseObject!.object(forKey: "storeId") as! CVarArg)
                        if storeId != "0"{
                            defaults .set(storeId, forKey: "storeId")
                        }
                    }
                    
                    GlobalData.sharedInstance.dismissLoader()
                    self.view.isUserInteractionEnabled = true
                    let data = responseObject as! NSDictionary
                    let errorCode: Bool = data .object(forKey:"success") as! Bool
                    
                    if errorCode == true{
                        GlobalData.sharedInstance.showSuccessSnackBar(msg: data.object(forKey: "message") as! String)
                    }else{
                        GlobalData.sharedInstance.showErrorSnackBar(msg:data.object(forKey: "message") as! String)
                    }
                }else if success == 2{
                    GlobalData.sharedInstance.dismissLoader()
                    self.callingExtraHttpApi()
                }
            }
        } else if whichApiToProcess == "deepLink" {
            //deep linking
            requstParams = [String:Any]()
            if defaults.object(forKey: "storeId") != nil{
                requstParams["storeId"] = defaults.object(forKey: "storeId") as! String
            }
            
            if let url = deepLinkUserInfo["url"] as? String{
                requstParams["url"] = url
            }else{
                requstParams["url"] = ""
            }
            
            
            requstParams["isFromUrl"] = true
            
            let apiName = "mobikulhttp/catalog/homePageData"
            GlobalData.sharedInstance.callingHttpRequest(params:requstParams, apiname:apiName, currentView: self){success,responseObject in
                if success == 1{
                    print(responseObject as! NSDictionary)
                    let dict =  JSON(responseObject as! NSDictionary)
                    self.view.isUserInteractionEnabled = true
                    GlobalData.sharedInstance.dismissLoader()
                    
                    if dict["success"].boolValue == true{
                        
                        if dict["productId"].stringValue != "0"{
                            //go to product page
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "catalogproduct") as! CatalogProduct
                            vc.productName = dict["productName"].stringValue
                            vc.productId = dict["productId"].stringValue
                            self.navigationController?.pushViewController(vc, animated: true)
                        }else if dict["categoryId"].stringValue != "0"{
                            //go to category page
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Productcategory") as! Productcategory
                            vc.categoryName = dict["categoryName"].stringValue
                            vc.categoryId = dict["categoryId"].stringValue
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }else if success == 2{
                    GlobalData.sharedInstance.dismissLoader()
                    self.callingHttppApi()
                }else if success == 0   {
                    if let data = responseObject as? NSDictionary   {
                        if let otherError = data.object(forKey: "otherError") as? String  , otherError == "customerNotExist"  {
                            print("customerNotExist call")
                            self.refreshControl.endRefreshing()
                            GlobalData.sharedInstance.dismissLoader()
                            defaults.removeObject(forKey: "customerId")
                            self.callingHttppApi()
                        }
                    }
                }
            }
        }
    }
    
    func showSuccessMessgae(data:String){
        
        let info = MessageView.viewFromNib(layout: .messageView)
        info.configureTheme(.success)
        info.button?.isHidden = false
        info.configureContent(title: GlobalData.sharedInstance.language(key: "success"), body: data, iconImage: nil, iconText: "👍", buttonImage: nil, buttonTitle: GlobalData.sharedInstance.language(key: "seelist")) { _ in
            SwiftMessages.hide()
            self.performSegue(withIdentifier: "comparelist", sender: self)
        }
        
        var infoConfig = SwiftMessages.defaultConfig
        infoConfig.presentationContext = .window(windowLevel: UIWindowLevelStatusBar)
        infoConfig.presentationStyle = .bottom
        infoConfig.dimMode = .blur(style: UIBlurEffectStyle.light, alpha: 0.4, interactive: true)
        infoConfig.duration = .forever
        SwiftMessages.show(config: infoConfig, view: info)
    }
    
    //MARK:- Recent Views Delegate func
    func recentProductClick(name: String, image: String, id: String) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "catalogproduct") as! CatalogProduct
        vc.productImageUrl = image
        vc.productName = name
        vc.productId = id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func recentAddToWishList(productID: String) {
        let customerId = defaults.object(forKey:"customerId");
        if customerId != nil{
            self.productId = productID;
            whichApiToProcess = "addtowishlist"
            callingExtraHttpApi()
        }else{
            let alertView = UIAlertController(title: "loginrequired".localized, message: "doyouwanttologin".localized, preferredStyle: .alert)
            let loginBtn = UIAlertAction(title: "loginalert".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "CustomerLogin") as? CustomerLogin
                self.navigationController?.pushViewController(vc!, animated: true)
            })
            let cancelBtn = UIAlertAction(title: "cancel".localized, style: .destructive, handler: {(_ action: UIAlertAction) -> Void in
            })
            
            alertView.addAction(cancelBtn)
            alertView.addAction(loginBtn)
            self.present(alertView, animated: true, completion: nil)
        }
    }
    
    func recentAddToCompare(productID: String) {
        self.productId = productID;
        whichApiToProcess = "addtocompare"
        callingExtraHttpApi()
    }
    
    func recentViewAllClick() {
        //do nothing
    }
    
    //MARK:-
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier! == "productcategory") {
            let viewController:Productcategory = segue.destination as UIViewController as! Productcategory
            viewController.categoryName = self.categoryName
            viewController.categoryId = self.categoryId
            viewController.categoryType = self.categoryType
        }
    }
}
