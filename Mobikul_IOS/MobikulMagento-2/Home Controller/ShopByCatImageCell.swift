//
//  ProductImageCell.swift
//  WooCommerce
//
//  Created by Webkul  on 04/11/17.
//  Copyright © 2017 Webkul . All rights reserved.
//

import UIKit

class ShopByCatImageCell: UICollectionViewCell {
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
}
