//
//  NewArriceViewCell.swift
//  MobikulMagento-2
//
//  Created by Sinn soklyhoi on 12/30/19.
//  Copyright © 2019 kunal. All rights reserved.
//

import Foundation
class PromotionViewCell: HotdealsTableViewCell {
    
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        headerTitle.text = GlobalData.sharedInstance.language(key: "promotion")
        collectionView.register(UINib(nibName: "PromotionImageCell", bundle: nil), forCellWithReuseIdentifier: "PromotionImageCell")
        collectionView.register(UINib(nibName: "ViewAllCell", bundle: nil), forCellWithReuseIdentifier: "ViewAllCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        self.collectionView.setCollectionViewLayout(layout, animated: true)
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/2 - 15 , height: collectionView.frame.size.width/2 + 70)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PromotionImageCell", for: indexPath) as! PromotionImageViewCell
        let extracell = collectionView.dequeueReusableCell(withReuseIdentifier: "ViewAllCell", for: indexPath) as! ViewAllCell
        cell.layer.cornerRadius = 8
        if indexPath.row < hotdealCollectionModel.count{ GlobalData.sharedInstance.getImageFromUrl(imageUrl:hotdealCollectionModel[indexPath.row].image , imageView: cell.productImage)
            cell.productName.text = hotdealCollectionModel[indexPath.row].name
            return cell
        } else {
            extracell.viewAllLabel.text = GlobalData.sharedInstance.language(key: "viewmore")
            return extracell
        }
    }
}
