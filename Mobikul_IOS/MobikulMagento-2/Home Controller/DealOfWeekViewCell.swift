//
//  NewArriceViewCell.swift
//  MobikulMagento-2
//
//  Created by Sinn soklyhoi on 12/30/19.
//  Copyright © 2019 kunal. All rights reserved.
//

import Foundation
class DealOfWeekViewCell: HotdealsTableViewCell {
    
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        headerTitle.text = GlobalData.sharedInstance.language(key: "dealOfWeeks")
        collectionView.register(UINib(nibName: "DealOfWeekImageCell", bundle: nil), forCellWithReuseIdentifier: "DealOfWeekImageCell")
        collectionView.register(UINib(nibName: "ViewAllCell", bundle: nil), forCellWithReuseIdentifier: "ViewAllCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        self.collectionView.setCollectionViewLayout(layout, animated: true)
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width - 20 , height: collectionView.frame.size.width - 80)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DealOfWeekImageCell", for: indexPath) as! DealOfWeekImageViewCell
        let extracell = collectionView.dequeueReusableCell(withReuseIdentifier: "ViewAllCell", for: indexPath) as! ViewAllCell
        cell.layer.cornerRadius = 8
        if indexPath.row < hotdealCollectionModel.count{
            GlobalData.sharedInstance.getImageFromUrl(imageUrl:hotdealCollectionModel[indexPath.row].image , imageView: cell.productImage)
            cell.productName.text = hotdealCollectionModel[indexPath.row].name
            cell.price.text = hotdealCollectionModel[indexPath.row].price
            if hotdealCollectionModel[indexPath.row].isInRange == 1{
                if hotdealCollectionModel[indexPath.row].specialPrice < hotdealCollectionModel[indexPath.row].originalPrice{
                    let attributeString = NSMutableAttributedString(string: ( hotdealCollectionModel[indexPath.row].formatedPrice ))
                    attributeString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 1, range: NSRange(location: 0, length: attributeString.length))
                    cell.disPrice.attributedText = attributeString
                    cell.disPrice.isHidden = false
                }else{
                    cell.disPrice.text = ""
                    cell.disPrice.isHidden = true
                }
            }else{
                cell.disPrice.text = ""
                cell.disPrice.isHidden = true
            }
            
            cell.disPrice.text = hotdealCollectionModel[indexPath.row].formatedPrice
            return cell
        } else {
            extracell.viewAllLabel.text = GlobalData.sharedInstance.language(key: "viewmore")
            return extracell
        }
    }
}
