//
//  NewArriverImageViewCell.swift
//  MobikulMagento-2
//
//  Created by Sinn soklyhoi on 12/30/19.
//  Copyright © 2019 kunal. All rights reserved.
//

import Foundation
class PromotionImageViewCell: UICollectionViewCell {
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var addToCart: UIButton!
    @IBOutlet weak var saleBtn: UIButton!
    
    
    override func awakeFromNib() {
    super.awakeFromNib()
        
        addToCart.layer.cornerRadius = addToCart.frame.height/2
        addToCart.layer.borderWidth = 1
        addToCart.layer.borderColor = addToCart.titleLabel?.textColor.cgColor
        saleBtn.layer.cornerRadius = 5
    }
}
