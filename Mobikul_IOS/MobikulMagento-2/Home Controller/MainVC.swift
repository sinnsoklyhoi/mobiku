//
//  MainVC.swift
//  MobikulMagento-2
//
//  Created by Sinn soklyhoi on 12/25/19.
//  Copyright © 2019 kunal. All rights reserved.
//

import UIKit
class MainVC: BaseVC {
    private var controllers = [ViewController(),ViewController()]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.addChildViewController(controllers[0])
        self.view.addSubview(controllers[0].view)
        controllers[0].didMove(toParentViewController: self)
    }
}
