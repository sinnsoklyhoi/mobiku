//
//  NewArriverImageViewCell.swift
//  MobikulMagento-2
//
//  Created by Sinn soklyhoi on 12/30/19.
//  Copyright © 2019 kunal. All rights reserved.
//

import Foundation
class NewArriverImageViewCell: UICollectionViewCell {
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var addToCart: UIButton!
    
    override func awakeFromNib() {
    super.awakeFromNib()
        
        addToCart.layer.cornerRadius = 10
        addToCart.layer.borderWidth = 1
        addToCart.layer.borderColor = addToCart.titleLabel?.textColor.cgColor
    }
}
