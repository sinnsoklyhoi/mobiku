//
//  HomeModel.swift
//  OpenCartMpV3
//
//  Created by Webkul on 11/12/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import UIKit

struct HomeModal {
    
    var bannerCollectionModel = [BannerData]()
    var latestProductCollectionModel = [Products]()
    var featuredProductCollectionModel = [Products]()
    var featureCategories = [FeatureCategories]()
    var languageData = [Languages]()
    var hotDealsProduct = [Products]()
    var shopByCategories = [Products]()
    var newArrivers = [Products]()
    var dealOfWeeks = [Products]()
    var promotions = [Products]()
    var cateoryData:[String:AnyObject]!
    var categoryImage = [CategoryImage]()
    var storeData = [StoreData]()
    var currency :NSArray!
    var cmsData = [CMSdata]()
    
    var recentViewData = [Productcollection]()
    
    //GDPR
    var gdprData : GDPRData!
    
    init?(data : JSON) {
        
        if var arrayData = data["bannerImages"].arrayObject{
            arrayData = (data["bannerImages"].arrayObject! as NSArray) as! [Any]
            bannerCollectionModel =  arrayData.map({(value) -> BannerData in
                return  BannerData(data:JSON(value))
            })
        }
        
        let arrayData2 = data["newProducts"].arrayObject! as NSArray
        latestProductCollectionModel =  arrayData2.map({(value) -> Products in
            return  Products(data:JSON(value))
        })
        let arrayData3 = data["featuredProducts"].arrayObject! as NSArray
        featuredProductCollectionModel =  arrayData3.map({(value) -> Products in
            return  Products(data:JSON(value))
        })
        
        let arrayData4 = data["featuredCategories"].arrayObject! as NSArray
        featureCategories =  arrayData4.map({(value) -> FeatureCategories in
            return  FeatureCategories(data:JSON(value))
        })
        
        let arrayData5 = data["hotDeals"].arrayObject! as NSArray
        hotDealsProduct =  arrayData5.map({(value) -> Products in
            return  Products(data:JSON(value))
        })
        shopByCategories = hotDealsProduct
        newArrivers = hotDealsProduct
        dealOfWeeks = hotDealsProduct
        promotions = hotDealsProduct
        
        let arrayData6 = data["categoryImages"].arrayObject! as NSArray
        categoryImage =  arrayData6.map({(value) -> CategoryImage in
            return  CategoryImage(data:JSON(value))
        })
        
        
        if var arrayData = data["storeData"].arrayObject{
            arrayData = (data["storeData"].arrayObject! as NSArray) as! [Any]
            storeData =  arrayData.map({(value) -> StoreData in
                return  StoreData(data:JSON(value))
            })
        }
        
        if data["allowedCurrencies"].arrayObject != nil{
            currency = data["allowedCurrencies"].arrayObject! as NSArray
        }
        
        if var arrayData = data["cmsData"].arrayObject{
            arrayData = (data["cmsData"].arrayObject! as NSArray) as! [Any]
            cmsData =  arrayData.map({(value) -> CMSdata in
                return  CMSdata(data:JSON(value))
            })
        }
        
        self.cateoryData = data["categories"].dictionaryObject! as [String : AnyObject]
        
        //GDPR
        self.gdprData = GDPRData(data: data)
        
        if data["customerBannerImage"].stringValue != ""{
            defaults.setValue(data["customerBannerImage"].stringValue, forKey: "profileBanner")
        }
        
        if data["customerProfileImage"].stringValue != ""{
            defaults.setValue(data["customerProfileImage"].stringValue, forKey: "profilePicture")
        }
        
        if data["customerEmail"].stringValue != ""{
            defaults.setValue(data["customerEmail"].stringValue, forKey: "customerEmail")
        }
        
        if data["customerName"].stringValue != ""{
            defaults.setValue(data["customerName"].stringValue, forKey: "customerName")
        }
        
        defaults.synchronize()
        
    }
}

struct BannerData{
    var bannerType:String!
    var imageUrl:String!
    var bannerId:String!
    var bannerName:String!
    var productId:String!
    
    init(data:JSON){
        bannerType = data["bannerType"].stringValue
        imageUrl  = data["url"].stringValue
        bannerId = data["categoryId"].stringValue
        bannerName = data["categoryName"].stringValue
        productId = data["productId"].stringValue
    }
}

struct FeatureCategories{
    var categoryID:String = ""
    var categoryName:String = ""
    var imageUrl:String = ""
    
    init(data:JSON) {
        self.categoryID = data["categoryId"].stringValue
        self.categoryName = data["categoryName"].stringValue
        self.imageUrl = data["url"].stringValue
    }
}

struct Products{
    
    var hasOption:Int!
    var name:String!
    var price:String!
    var productID:String!
    var showSpecialPrice:String!
    var image:String!
    var isInRange:Int!
    var isInWishList:Bool!
    var originalPrice:Double!
    var specialPrice:Double!
    var formatedPrice:String!
    var typeID:String!
    var groupedPrice:String!
    var formatedMinPrice:String!
    var formatedMaxPrice:String!
    var wishlistItemId:String!
    var shortDescription:String = ""
    var requiredOptions:Int = 0
    
    init(data:JSON) {
        self.hasOption = data["hasOption"].intValue
        self.name = data["name"].stringValue
        self.price = data["formatedFinalPrice"].stringValue
        self.productID = data["entityId"].stringValue
        self.showSpecialPrice = data["formatedFinalPrice"].stringValue
        self.image = data["thumbNail"].stringValue
        self.originalPrice =  data["price"].doubleValue
        self.specialPrice = data["finalPrice"].doubleValue
        self.isInRange = data["isInRange"].intValue
        self.isInWishList = data["isInWishlist"].boolValue
        self.formatedPrice = data["formatedPrice"].stringValue
        self.typeID = data["typeId"].stringValue
        self.groupedPrice = data["groupedPrice"].stringValue
        self.formatedMaxPrice = data["formatedMaxPrice"].stringValue
        self.formatedMinPrice = data["formatedMinPrice"].stringValue
        self.wishlistItemId = data["wishlistItemId"].stringValue
        self.shortDescription = data["shortDescription"].stringValue.html2String
        self.requiredOptions = data["requiredOptions"].intValue
    }
}

struct StoreData{
    var id:String = ""
    var name:String = ""
    var stores = [Languages]()
    
    init(data:JSON) {
        self.id = data["id"].stringValue
        self.name = data["name"].stringValue
        
        if let arrayData = data["stores"].arrayObject{
            stores =  arrayData.map({(value) -> Languages in
                return  Languages(data:JSON(value))
            })
        }
    }
}

struct Languages {
    var code:String!
    var name:String!
    var id:String!
    
    init(data:JSON){
        self.code = data["code"].stringValue
        self.name = data["name"].stringValue
        self.id = data["id"].stringValue
    }
}

struct CategoryImage{
    var bannerImage:String = ""
    var id:String = ""
    var iconImage:String = ""
    
    init(data:JSON) {
        self.bannerImage = data["banner"].stringValue
        self.id = data["id"].stringValue
        self.iconImage = data["thumbnail"].stringValue
    }
}

struct CMSdata{
    var id:String!
    var title:String!
    
    init(data:JSON) {
        self.id = data["id"].stringValue
        self.title = data["title"].stringValue
    }
}

struct GDPRData {
    var gdprEnable : Bool = false
    var tncHomepageEnable : Bool = false
    var tncHomepageContent : String = ""
    
    init(data:JSON) {
        gdprEnable = data["gdprEnable"].boolValue
        tncHomepageEnable = data["tncHomepageEnable"].boolValue
        tncHomepageContent = data["tncHomepageContent"].stringValue
    }
}

enum HomeViewModelItemType {
    
    //case Category
    case Shop_In_Categories
    case Banner
    case FeatureCategory
    case LatestProduct
    case FeatureProduct
    case RecentViewData
    case hotDeal
    case NewArriver
    case DealOfTheWeek
    case Promotion
}

protocol HomeViewModelItem {
    var type: HomeViewModelItemType { get }
    var sectionTitle: String { get }
    var rowCount: Int { get }
}

class HomeViewModelBannerItem: HomeViewModelItem {
    var type: HomeViewModelItemType {
        return .Banner
    }
    
    var sectionTitle: String {
        return ""
    }
    
    var rowCount: Int {
        return bannerCollectionModel.count
    }
    
    var bannerCollectionModel = [BannerData]()
    
    init(categories: [BannerData]) {
        self.bannerCollectionModel = categories
    }
}


class HomeViewModelFeatureCategoriesItem: HomeViewModelItem {
    var type: HomeViewModelItemType {
        return .FeatureCategory
    }
    
    var sectionTitle: String {
        return ""
    }
    
    var rowCount: Int {
        return featureCategories.count
    }
    
    var featureCategories = [FeatureCategories]()
    
    init(categories: [FeatureCategories]) {
        self.featureCategories = categories
    }
}

class HomeViewModelLatestItem: HomeViewModelItem {
    var type: HomeViewModelItemType {
        return .LatestProduct
    }
    
    var sectionTitle: String {
        return ""
    }
    
    var rowCount: Int {
        return latestProductCollectionModel.count
    }
    
    var latestProductCollectionModel = [Products]()
    
    init(categories: [Products]) {
        self.latestProductCollectionModel = categories
    }
    
}

class HomeViewModelFeatureItem: HomeViewModelItem {
    var type: HomeViewModelItemType {
        return .FeatureProduct
    }
    
    var sectionTitle: String {
        return ""
    }
    
    var rowCount: Int {
        return featuredProductCollectionModel.count
    }
    
    var featuredProductCollectionModel = [Products]()
    
    init(categories: [Products]) {
        self.featuredProductCollectionModel = categories
    }
}

class HomeViewModelRecentViewItem: HomeViewModelItem    {
    var type: HomeViewModelItemType {
        return .RecentViewData
    }
    
    var sectionTitle: String {
        return ""
    }
    
    var rowCount: Int {
        return recentViewProductData.count
    }
    
    var recentViewProductData = [Productcollection]()
    
    init(categories: [Productcollection]) {
        self.recentViewProductData = categories
    }
}

class HomeViewModelHotdealItem: HomeViewModelItem {
    var type: HomeViewModelItemType {
        return .hotDeal
    }
    
    var sectionTitle: String {
        return ""
    }
    
    var rowCount: Int {
        return hotDealsProduct.count
    }
    
    var hotDealsProduct = [Products]()
    
    init(categories: [Products]) {
        self.hotDealsProduct = categories
    }
}

class HomeViewModelShopByCategories: HomeViewModelItem {
    var sectionTitle: String {
        return ""
    }
    
    var shopCat = [Products]()
    init(categories: [Products]) {
        shopCat = categories
    }
    
    var rowCount: Int {
        return shopCat.count
    }
    
    var type: HomeViewModelItemType {
        return .Shop_In_Categories
    }
}

class HomeViewModelNewArriver: HomeViewModelItem {
    var sectionTitle: String {
        return ""
    }
    
    var newArriver = [Products]()
    init(categories: [Products]) {
        newArriver = categories
    }
    
    var rowCount: Int {
        return newArriver.count
    }
    
    var type: HomeViewModelItemType {
        return .NewArriver
    }
}

class HomeViewModelDealOfWeek: HomeViewModelItem {
    var sectionTitle: String {
        return ""
    }
    
    var dealOfWeek = [Products]()
    init(categories: [Products]) {
        dealOfWeek = categories
    }
    
    var rowCount: Int {
        return dealOfWeek.count
    }
    
    var type: HomeViewModelItemType {
        return .DealOfTheWeek
    }
}

class HomeViewModelPromotion: HomeViewModelItem {
    var sectionTitle: String {
        return ""
    }
    
    var promotions = [Products]()
    init(categories: [Products]) {
        promotions = categories
    }
    
    var rowCount: Int {
        return promotions.count
    }
    
    var type: HomeViewModelItemType {
        return .Promotion
    }
}

class HomeViewModel : NSObject {
    var items = [HomeViewModelItem]()
    var featuredProductCollectionModel = [Products]()
    var letestProductCollectionModel = [Products]()
    var cateoryData:[String:AnyObject]!
    
    var currency :NSArray = []
    var languageData = [Languages]()
    var homeViewController:ViewController!
    var guestCheckOut:Bool!
    var categoryImage = [CategoryImage]()
    var storeData = [StoreData]()
    var cmsData = [CMSdata]()
    
    //GDPR
    var gdprData : GDPRData!
    
    func getData(data : AnyObject , recentViewData : [Productcollection] , completion:(_ data: Bool) -> Void) {
        guard let data = HomeModal(data: JSON(data as! NSDictionary)) else {
            return
        }
        items.removeAll()
        if !data.bannerCollectionModel.isEmpty {
            let bannerDataCollectionItem = HomeViewModelBannerItem(categories: data.bannerCollectionModel)
            items.append(bannerDataCollectionItem)
            
        }
        if !data.featureCategories.isEmpty {
            let featureCategoryCollectionItem = HomeViewModelFeatureCategoriesItem(categories: data.featureCategories)
            items.append(featureCategoryCollectionItem)
            
        }
        if !data.featuredProductCollectionModel.isEmpty {
            featuredProductCollectionModel = data.featuredProductCollectionModel
        }
        
        let latestCollectionItem = HomeViewModelLatestItem(categories: data.latestProductCollectionModel)
        items.append(latestCollectionItem)
        letestProductCollectionModel = data.latestProductCollectionModel
        
        if !recentViewData.isEmpty {
            let recentViewCollectionItem = HomeViewModelRecentViewItem(categories: recentViewData)
            items.append(recentViewCollectionItem)
        }
        
        if !data.hotDealsProduct.isEmpty {
            let hotDealCollectionItem = HomeViewModelHotdealItem(categories: data.hotDealsProduct)
            items.append(hotDealCollectionItem)
        }
        
        if !data.shopByCategories.isEmpty {
            let shopByCat = HomeViewModelShopByCategories(categories: data.hotDealsProduct)
            items.append(shopByCat)
        }
        
        if !data.newArrivers.isEmpty {
            let newArrivers = HomeViewModelNewArriver(categories: data.hotDealsProduct)
            items.append(newArrivers)
        }
        
        if !data.dealOfWeeks.isEmpty {
            let dealOfWeeks = HomeViewModelDealOfWeek(categories: data.hotDealsProduct)
            items.append(dealOfWeeks)
        }
        
        if !data.dealOfWeeks.isEmpty {
            let promotions = HomeViewModelPromotion(categories: data.hotDealsProduct)
            items.append(promotions)
        }
        
        if data.currency != nil{
            self.currency = data.currency
        }
        
        if data.cmsData != nil{
            self.cmsData = data.cmsData
        }
        
        if data.gdprData != nil{
            self.gdprData = data.gdprData
        }
        
        self.categoryImage = data.categoryImage
        self.cateoryData = data.cateoryData
        self.storeData = data.storeData
        
        
        completion(true)
    }
    
    
    
    func setWishListFlagToFeaturedProductModel(data:Bool,pos:Int){
        featuredProductCollectionModel[pos].isInWishList = data
    }
    
    func setWishListItemIdToFeaturedProductModel(data:String,pos:Int){
        featuredProductCollectionModel[pos].wishlistItemId = data
    }
    
    
    func setWishListFlagToLatestProductModel(data:Bool,pos:Int){
        letestProductCollectionModel[pos].isInWishList = data
    }
    
    func setWishListItemIdToLatestProductModel(data:String,pos:Int){
        letestProductCollectionModel[pos].wishlistItemId = data
    }
}



extension HomeViewModel : UITableViewDelegate , UITableViewDataSource {
    public func numberOfSections(in tableView: UITableView) -> Int{
        return items.count 
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.section]
        
        switch item.type {
        case .Banner:
            let cell:BannerTableViewCell = tableView.dequeueReusableCell(withIdentifier: BannerTableViewCell.identifier) as! BannerTableViewCell
            cell.delegate = homeViewController
            cell.bannerCollectionModel = ((item as? HomeViewModelBannerItem)?.bannerCollectionModel)!
            
            cell.selectionStyle = .none
            return cell
            
        case .FeatureCategory:
//            let cell:TopCategoryTableViewCell = tableView.dequeueReusableCell(withIdentifier: TopCategoryTableViewCell.identifier) as! TopCategoryTableViewCell
//            cell.featureCategoryCollectionModel = ((item as? HomeViewModelFeatureCategoriesItem)?.featureCategories)!
//            cell.categoryCollectionView.reloadData()
//            cell.delegate = homeViewController
//
//            cell.selectionStyle = .none
//            return cell
            return UITableViewCell()
        case .FeatureProduct:
            return UITableViewCell()
            
        case .RecentViewData:
//            let cell:RecentViewTableViewCell = tableView.dequeueReusableCell(withIdentifier: RecentViewTableViewCell.identifier) as! RecentViewTableViewCell
//            cell.recentCollectionModel = ((item as? HomeViewModelRecentViewItem)?.recentViewProductData)!
//            cell.recentViewCollectionView.reloadData()
//            cell.delegate = homeViewController
//
//            cell.selectionStyle = .none
//            return cell
            return UITableViewCell()
            
        case .hotDeal:
//            let cell:HotdealsTableViewCell = tableView.dequeueReusableCell(withIdentifier: HotdealsTableViewCell.identifier) as! HotdealsTableViewCell
//            cell.hotdealCollectionModel = ((item as? HomeViewModelHotdealItem)?.hotDealsProduct)!
//            cell.hotdelalCollectionView.reloadData()
//            cell.delegate = homeViewController
//
//            cell.selectionStyle = .none
//            return cell
            return UITableViewCell()
        case .Shop_In_Categories:
           let cell:ShopByCategoriesViewCell = tableView.dequeueReusableCell(withIdentifier: ShopByCategoriesViewCell.identifier) as! ShopByCategoriesViewCell
            cell.hotdealCollectionModel = ((item as? HomeViewModelShopByCategories)?.shopCat)!
            cell.hotdelalCollectionView.reloadData()
            cell.delegate = homeViewController
            
            cell.selectionStyle = .none
            return cell
        case .NewArriver:
            let cell:NewArriverViewCell = tableView.dequeueReusableCell(withIdentifier: NewArriverViewCell.identifier) as! NewArriverViewCell
            cell.hotdealCollectionModel = ((item as? HomeViewModelNewArriver)?.newArriver)!
            cell.hotdelalCollectionView.reloadData()
            cell.delegate = homeViewController
            
            cell.selectionStyle = .none
            return cell
        case .DealOfTheWeek:
            let cell:DealOfWeekViewCell = tableView.dequeueReusableCell(withIdentifier: DealOfWeekViewCell.identifier) as! DealOfWeekViewCell
            cell.hotdealCollectionModel = ((item as? HomeViewModelDealOfWeek)?.dealOfWeek)!
            cell.hotdelalCollectionView.reloadData()
            cell.delegate = homeViewController
            
            cell.selectionStyle = .none
            return cell
        case .Promotion:
            let cell:PromotionViewCell = tableView.dequeueReusableCell(withIdentifier: PromotionViewCell.identifier) as! PromotionViewCell
            cell.hotdealCollectionModel = ((item as? HomeViewModelPromotion)?.promotions)!
            cell.hotdelalCollectionView.reloadData()
            cell.delegate = homeViewController
            
            cell.selectionStyle = .none
            return cell
            
        case .LatestProduct:
            let cell:ProductTableViewCell = tableView.dequeueReusableCell(withIdentifier: ProductTableViewCell.identifier) as! ProductTableViewCell
            cell.prodcutCollectionView.reloadData()
            cell.prodcutCollectionView.tag = indexPath.section
            cell.productCollectionModel = ((item as? HomeViewModelLatestItem)?.latestProductCollectionModel)!
            cell.delegate = homeViewController
            cell.featuredProductCollectionModel = featuredProductCollectionModel
            cell.homeViewController = homeViewController
            cell.homeViewModel = homeViewController.homeViewModel
            
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = items[indexPath.section]
        switch item.type {
        case .Banner:
            return SCREEN_WIDTH / 2
            
        case .FeatureCategory:
            return 0//120
            
        case .LatestProduct:
            return UITableViewAutomaticDimension
            
        case .FeatureProduct:
            return 0
            
        case .RecentViewData:
            return 0//SCREEN_WIDTH / 2 + 140
            
        case .hotDeal:
            return 0//SCREEN_WIDTH / 2 + 140
        case .Shop_In_Categories:
            return SCREEN_WIDTH / 2 + 130
        case .NewArriver:
            return SCREEN_WIDTH / 2 + 140
        case .DealOfTheWeek:
            return SCREEN_WIDTH
        case .Promotion:
            return SCREEN_WIDTH / 2 + 130
        }
    }
}
