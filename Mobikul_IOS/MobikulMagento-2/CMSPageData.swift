//
//  CMSPageData.swift
//  Ajmal
//
//  Created by Webkul on 05/05/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit

class CMSPageData: UIViewController{
    
    public var cmsId:String!
    public var cmsName:String!
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController!.isNavigationBarHidden = false
        self.callingHttppApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController!.isNavigationBarHidden = false
    }
    
    func callingHttppApi(){
        GlobalData.sharedInstance.showLoader()
        var requstParams = [String:Any]()
        requstParams["id"] = cmsId
        GlobalData.sharedInstance.callingHttpRequest(params:requstParams, apiname:"mobikulhttp/extra/cmsData", currentView: self){success,responseObject in
            if success == 1{
                print("sss", responseObject)
                self.doFurtherProcessingWithResult(data: (responseObject as! NSDictionary))
                
            }else if success == 2{
                self.callingHttppApi()
            }
        }
    }
    
    func doFurtherProcessingWithResult(data :NSDictionary){
        DispatchQueue.main.async {
            self.view.isUserInteractionEnabled = true
            GlobalData.sharedInstance.dismissLoader()
            
            if let heading = data.object(forKey: "title") as? String    {
                self.navigationController?.navigationBar.topItem?.title = heading
            }

            self.webView.loadHTMLString(data.object(forKey: "content") as! String, baseURL: nil)
        }
    }
}
